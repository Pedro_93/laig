%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%
%-%-%-%-%-%-%-%						 				MAIN    		  		     		%-%-%-%-%-%-%-%
%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%                 Sockets                   %%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

:-use_module(library(sockets)).

port(60070).

% launch me in sockets mode
server:-
	port(Port),
	socket_server_open(Port, Socket),
	socket_server_accept(Socket, _Client, Stream, [type(text)]),
	write('Accepted connection'), nl,
	serverLoop(Stream),
	socket_server_close(Socket).

% wait for commands
serverLoop(Stream) :-
	repeat,
	read(Stream, ClientMsg),
	write('Received: '), write(ClientMsg), nl,
	parse_input(ClientMsg, MyReply),
	format(Stream, '~q.~n', [MyReply]),
	write('Wrote: '), write(MyReply), nl,
	flush_output(Stream),
	(ClientMsg == quit; ClientMsg == end_of_file), !.

parse_input(comando(Arg1, Arg2), Answer) :-
	comando(Arg1, Arg2, Answer).
			
parse_input(initialize(Arg1), Answer):- 
	write(Arg1),nl,
	criaTabuleiro( Arg1 , Arg1 , [] , Answer ),!.
	
parse_input(setPeca(Tab,Arg1,Arg2,Arg3), Answer):-
	write('ola'),nl,
	setPeca(Tab,Arg1,Arg2,Arg3,Answer),!.
	
parse_input(validate(Tab,Arg1,Arg2,Arg3,Arg4,Arg5,Arg6,Arg7),Answer):-
	jogadaValida(Tab,Arg1,[Arg2,Arg3],[Arg4,Arg5],Arg6,Arg7,Answer),
	write('output: '),write(Answer),nl,!.
parse_input(validate(_,_,_,_,_,_,_,_),-1):- write('Invalid move'),nl,!.


parse_input(validateBonusJump(Tab,Arg1,Arg2,Arg3,Arg4,Arg5,Arg6,Arg7),Answer):-
	jogadaValidaAux(Tab,Arg1,[Arg2,Arg3],[Arg4,Arg5],Arg6,Arg7,Answer) ,
	write('output: '),write(Answer),nl,!.
parse_input(validateBonusJump(_,_,_,_,_,_,_,_),-1):- write('Invalid move'),nl,!.

parse_input(validateBonusSwitch(Tab,Arg4,Arg5,Arg7),Answer):-
	bonusTrocaPecas(Tab,[Arg4,Arg5],Arg7,Answer) ,
	write('output: '),write(Answer),nl,!.
parse_input(validateBonusSwitch(_,_,_,_),-1):- write('Invalid move'),nl,!.
	
parse_input(fimJogo(Tab,Arg1,Arg2,Arg3,Arg4,Arg5,Player),Answer):-
	validaFimJogo(Tab,Arg1,Arg2,Arg3,Arg4,Arg5,Answer,Player),!.

	
parse_input(execute(Mov, Board), ok(NewBoard)):- 
	valid_move(Mov, Board),
	execute_move(Mov, Board, NewBoard), !.
	
parse_input(calculate(Level, J, Board), ok(Mov, NewBoard)):- 
	calculate_move(Level, J, Board, Mov),
	execute_move(Mov, Board, NewBoard), !.
	
parse_input(game_end(Board), ok(Winner)):- 
	end_game(Board, Winner), !.
	
parse_input(quit, ok-bye) :- !.
	
parse_input(end_of_file, ok-bye):-!.

parse_input(_, invalid) :- !.

comando(Arg1, Arg2, Answer) :-
	write(Arg1), nl, write(Arg2), nl,
	Answer = [[4],
     [1,2,3,3,2,1],
     [0,0,0,0,0,0],
     [0,0,0,0,0,0],
     [0,0,0,0,0,0],
     [0,0,0,0,0,0],
     [1,2,3,3,2,1],
     [4]
    ].

%-----------------------------------------------------------%
% 					Chamada inicial do jogo
%-----------------------------------------------------------%
iniciarJogo:-
    cabecalhoJogo ,
    tamanhoTabuleiroEscolhido( N ) ,!,
   % modoJogo(J1,J2),
    criaTabuleiro( N , N , [] , Tabuleiro ) ,
    desenhaTabuleiro1( Tabuleiro , 4 ) ,
    dispoePecas( Tabuleiro , N , NovoTabuleiroJogo ) ,
	play( NovoTabuleiroJogo , N ).

%-----------------------------------------------------------%
% 			Cabecalho de identificacao do jogo
%-----------------------------------------------------------%
cabecalhoJogo:-
    nl, 
    write('/-------------------------------------------------|'), nl,
    write('|------------------::: GYGES :::------------------|'), nl,
    write('|-------------------------------------------------|'), nl,
    write('|      created by Luis Abreu and Pedro Silva      |'), nl,
    write('|                PLOG   2013/2014                 |'), nl,
    write('|-------------------------------------------------/'), nl, nl.


%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%
%-%-%-%-%-%-%-%						 	INPUTS DO UTILIZADOR    	  		     		%-%-%-%-%-%-%-%
%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%

%-----------------------------------------------------------%
% 	Escolha do tamanho do tabuleiro :: tamanho válido
%-----------------------------------------------------------%
tamanhoTabuleiroEscolhido(N):-
    nl, write('Escolha um multiplo de 3 entre 6 e 18, para definir o tamanho do tabuleiro:'),
    read(X), number(X), 
    inputIntValido(X), 
    N is X.
tamanhoTabuleiroEscolhido(N):-
    write('Input invalido. Tente novamente.'),
    tamanhoTabuleiroEscolhido(N).

inputIntValido(X):-
    X>=6 , X=<18 ,
    Result is X mod 3 ,
    Result =:= 0 .

modoJogo(Jogador1, Jogador2):-
	write('Escolha o modo de jogo que pretende (Jogador(Norte) vs Jogador(Sul)):'),nl,
	write('1 - Humano vs Humano'),nl,
	write('2 - Humano vs PC'),nl,
	write('3 - PC vs Humano'),nl,
	write('4 - PC vs PC'),nl,
	read(X),number(X),
	X>=1 , X=<4,!,
	setJogadores(Jogador1,Jogador2,X).

modoJogo(Jogador1,Jogador2):-
	write('Input invalido, tente outra vez'),nl,
	modoJogo(Jogador1, Jogador2).

dificuldade(Resultado):-
	write('Escolha o nivel de inteligencia do computador que pretende (1,2,3):'),nl,
	read(X),number(X),
	X>=1 , X=<3,
	Resultado is X,!.

dificuldade(Resultado):-
	write('Input invalido, tente outra vez'),nl,
	dificuldade(Resultado).

setJogadores(Jogador1,Jogador2,1):-
	Jogador1 is 0,
	Jogador2 is 0,
	write('Modo Jogo: Humano vs Humano'),nl.

setJogadores(Jogador1,Jogador2,2):-
	dificuldade(Resultado),
	Jogador1 is 0,
	Jogador2 is 1,
	write('Modo Jogo: Humano vs PC'),nl,
	write('Pedimos desculpa mas nao temos o predicado calculaJogada(dificuldade:'),write(Resultado),write(', Jogador:PC, Tabuleiro)'),fail.

setJogadores(Jogador1,Jogador2,3):-
	dificuldade(Resultado),
	Jogador1 is 1,
	Jogador2 is 0,
	write('Modo Jogo: PC vs Humano'),nl,
	write('Pedimos desculpa mas nao temos o predicado calculaJogada(dificuldade:'),write(Resultado),write(', Jogador:PC, Tabuleiro)'),fail.

setJogadores(Jogador1,Jogador2,4):-
	dificuldade(Resultado),
	Jogador1 is 1,
	Jogador2 is 1,
	write('Modo Jogo: PC vs PC'),nl,
	write('Pedimos desculpa mas nao temos o predicado calculaJogada(dificuldade:'),write(Resultado),write(', Jogador:PC, Tabuleiro)'),fail.


%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%
%-%-%-%-%-%-%-%						 		COLOCACAO DE PECAS    	  		     		%-%-%-%-%-%-%-%
%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%

%-----------------------------------------------------------%
% O utilizador escolhe onde colocar as pecas no tabuleiro
%-----------------------------------------------------------%
% -<>-O numero de pecas depende do tamanho do tabuleiro-<>- %

dispoePecas( Tabuleiro , Dimensao , NovoTabuleiroJogo ):-
	N is Dimensao//3 ,
	cicloDisposicao( Tabuleiro , Dimensao , 0 , [N,N,N] , TabuleiroAuxiliar , 1 ) ,
	cicloDisposicao( TabuleiroAuxiliar , Dimensao , 0 , [N,N,N] , NovoTabuleiroJogo , 2 ) .
	
cicloDisposicao( Tabuleiro , Dimensao , NumeroPecas , Pecas , NovoTabuleiroJogo , Jogador ):-
	NumeroPecas < Dimensao ,
	escolhePeca(Pecas , EscolhaPeca , NovasPecas ) ,
	escolheposicao( Tabuleiro , Dimensao , EscolhaPosicao , Jogador ) , ! ,
	modificaTabuleiro( Tabuleiro , EscolhaPeca , EscolhaPosicao , NovoTabuleiro , Jogador ) ,
	N is NumeroPecas +1 ,
	desenhaTabuleiro1(NovoTabuleiro , 4), nl,
	cicloDisposicao( NovoTabuleiro, Dimensao , N , NovasPecas , NovoTabuleiroJogo , Jogador ).

cicloDisposicao( Tabuleiro , _ , _ , _ , NovoTabuleiroJogo , _ ):-
	NovoTabuleiroJogo = Tabuleiro .


%-----------------------------------------------------------%
% 	Update do tabuleiro de jogo com novas pecas
%-----------------------------------------------------------%
modificaTabuleiro( [H|T] , EscolhaPeca , EscolhaPosicao , NovoTabuleiro , Jogador ):-
	Jogador =:= 1 , %(% jogador 1 == jogador a norte %)%
	LinhaAtiva = H ,
	alteraLista( LinhaAtiva , EscolhaPosicao , EscolhaPeca , NovaLista , [] ) ,
	cria_lista( NovaLista , [] , ListaNovoH ) ,
	append( ListaNovoH , T , TabuleiroNovo ) ,
	NovoTabuleiro = TabuleiroNovo .

modificaTabuleiro( Lista , EscolhaPeca , EscolhaPosicao , NovoTabuleiro , Jogador ):-
	Jogador =:= 2 , %(% jogador 2 == jogador a sul %)%
	inverte_lista_listas( Lista , [H1|T1] ) ,
	LinhaAtiva = H1 ,
	alteraLista( LinhaAtiva , EscolhaPosicao , EscolhaPeca , NovaLista , [] ) ,
	cria_lista( NovaLista , [] , ListaNovoH ) ,
	append( ListaNovoH , T1 , TabuleiroNovo ) ,
	inverte_lista_listas( TabuleiroNovo , TabuleiroFinal ) ,
	NovoTabuleiro = TabuleiroFinal .
	
alteraLista( [H|T] , Posicao , Valor , NovaLista , ListaAux ):-
	Posicao > 1 ,
	N is Posicao - 1 , 
	cria_lista( H , ListaAux , ListaH ) ,
	alteraLista( T , N , Valor , NovaLista , ListaH ) .

alteraLista( [_|T] , _ , Valor , NovaLista , ListaAux ):-
	cria_lista( Valor , ListaAux , ListaH ) ,
	inverte_lista( ListaH , ListaH1 ) ,
	append( ListaH1 , T , NovaLista ) .

%-----------------------------------------------------------%
% 	Escolha da posicao para colocar peca no tabuleiro
%-----------------------------------------------------------%
				
escolheposicao( Tabuleiro , Dimensao , EscolhaPosicao , Jogador ):-
	nl, write('Escolha uma posicao em x para colocar a peca escolhida: '),
	read(X) , number(X), posicaoValida( Tabuleiro , Dimensao , X , Jogador ) ,
	EscolhaPosicao is X .
	
escolheposicao( Tabuleiro , Dimensao , EscolhaPosicao , Jogador ):-
	write('Posicao invalida. Tente novamente.'), nl,
	escolheposicao( Tabuleiro , Dimensao , EscolhaPosicao , Jogador ) .
	
posicaoValida( Tabuleiro , Dimensao , Posicao , Jogador ):-
	%(% verifica a validade do input %)%
	Posicao>=1 , Posicao=<Dimensao ,
	Result is Posicao mod 1 ,
	Result =:= 0 ,
	%(% verifica se a posicao se encontra livre %)%
	verificaDisponibilidadePosicao( Tabuleiro , Dimensao , Posicao , Jogador ).	
	
% -<>- [H|T] representa o tabuleiro de jogo -<>- %
% -<>-<>- o jogador a norte modifica a primeira linha do tabuleiro
% enquanto que o jogador a sul modifica a última, por esta razão,
% na segunda versão do predicado é feita uma inversão da lista
% -<>-<>-
verificaDisponibilidadePosicao( [H|_] , Dimensao , Posicao , Jogador ):-
	Jogador =:= 1 , %(% jogador 1 == jogador a norte %)%
	LinhaAtiva = H ,
	nth_elemento( Posicao, Dimensao, LinhaAtiva , Elemento ) ,
	Elemento =:= 0 .
	
verificaDisponibilidadePosicao( [H|T] , Dimensao , Posicao , Jogador ):-
	Jogador =:= 2 , %(% jogador 2 == jogador a sul %)%
	inverte_lista_listas( [H|T] , [H1|_] ) ,
	LinhaAtiva = H1 ,
	nth_elemento( Posicao, Dimensao, LinhaAtiva , Elemento ) ,
	Elemento =:= 0 .

	
%-----------------------------------------------------------%
% 		Escolha de uma peca para colocar no tabuleiro
%-----------------------------------------------------------%
% -<>- O utilizador escolhe um numero e se for uma das
% opcoes, a lista de pecas atualiza decrementando a peca
% usada caso seja possivel. Caso contrario pede outro input
% -<>-

escolhePeca(Pecas , EscolhaPeca , NovasPecas ):-
	imprimeListaDePecas(Pecas) ,
    nl, write('Escolha uma peca segundo a numeracao da esquerda: ') ,
    read(X) , 
    number(X) , 
    pecaValida(X, Pecas, NovasPecas) ,
    EscolhaPeca is X .

escolhePeca(Pecas , EscolhaPeca , NovasPecas ):-
    write('Input invalido. Tente novamente.'), nl,
    escolhePeca(Pecas , EscolhaPeca , NovasPecas ).

pecaValida(X, Pecas , NovasPecas):-
    X>=1 , X=<3 , 		%(% as opcoes sao 1, 2 ou 3 %)%
    Result is X mod 1 , %(% fica 'true' no caso em que X=:=1 ou =:=2 ou =:=3 %)%
    Result =:= 0 ,
    %(% agora se passou ate aqui, precisamos de ver se ainda existem pecas do tipo escolhido %)%
    atualizaPecas( X , Pecas , NovasPecas ) .


% -<>- O predicado atualizaPecas interpreta apenas 3 valores para X : 1,2,3
% Caso existam pecas disponiveis, decreementa o numero de pecas e cria 
% a lista NovasPecas com os valores atualizados
% -<>-
atualizaPecas(X, [H1|[H2|H3]], NovasPecas ):-
	X =:= 1 ,
	H1 > 0 ,
	N is H1 - 1 ,
	cria_lista( H2 , H3 , ListaAuxiliar ) ,
	cria_lista( N , ListaAuxiliar , NovasPecas) .
atualizaPecas(X, [H1|[H2|H3]], NovasPecas ):-
	X =:= 2 ,
	H2 > 0 ,
	N is H2 - 1 ,
	cria_lista( N , H3 , ListaAuxiliar ) ,
	cria_lista( H1 , ListaAuxiliar , NovasPecas) .
atualizaPecas(X, [H1|[H2|H3]], NovasPecas ):-
	X =:= 3 ,
	H3 > 0 ,
	N is H3 - 1 ,
	cria_lista( H2 , [N] , ListaAuxiliar ) ,
	cria_lista( H1 , ListaAuxiliar , NovasPecas) .

%-----------------------------------------------------------%
% 	Imprime um lista com o tipo de peca e o numero em mao
%-----------------------------------------------------------%
% -<>- Uma lista de pecas tem 3 valores, sendo que cada um 
% corresponde a um tipo de peca pela ordem: simples, dupla, 
% tripla 
% -<>-
imprimeListaDePecas( [H1|[H2|[H3|_]]] ):-
	write('-- Pecas Disponiveis --') , nl ,
	write('<1> Simples (') , write(H1) , write(')') , nl ,
	write('<2> Duplas  (') , write(H2) , write(')') , nl ,
	write('<3> Triplas (') , write(H3) , write(')') , nl .

	
%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%
%-%-%-%-%-%-%-%						 		IMPRIME TABULEIRO    	  		     		%-%-%-%-%-%-%-%
%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%

%-----------------------------------------------------------%
% 	  Main da imprecao :: desenha cabecalho e tabuleiro
%-----------------------------------------------------------%
desenhaTabuleiro1( Tabuleiro , GoalVal ):-
	length( Tabuleiro , Tamanho) ,
	NEspacos is (((Tamanho/2)*3)-2) ,
	write('   | ') ,
	desenhaCabecalhoTabuleiro( Tamanho ) , nl ,
		write(' 0 | ') , desenhaGoal( NEspacos , GoalVal ) , nl , 
	desenhaTabuleiro2( Tabuleiro , 1 ) ,
		N is Tamanho+1 , write(' ') , write(N) , write(' | ') , desenhaGoal( NEspacos , GoalVal ) , nl .

% -<>- desenha a linha goal do tabuleiro -<>- %
desenhaGoal( N , C ):- N>0 , write(' ') , N1 is N-1 , desenhaGoal( N1 , C) .
desenhaGoal( _ , C ):- escreveSimbolo(C) .
	
%-----------------------------------------------------------%
% Imprime o tabuleiro com a numeracao das diferentes linhas
%-----------------------------------------------------------%
desenhaTabuleiro2( [] , _ ).
desenhaTabuleiro2( [H|T] , N ):-
	N < 10 ,
	write(' ') , write(N) , write(' | ') , desenhaLinha(H) ,
	N1 is N + 1 ,
	desenhaTabuleiro2(T , N1).
desenhaTabuleiro2( [H|T] , N ):-
	write(N) , write(' | ') , desenhaLinha(H) ,
	N1 is N + 1 ,
	desenhaTabuleiro2(T , N1).

%-----------------------------------------------------------%
% Imprime o tabuleiro com a numeracao das diferentes linhas
%-----------------------------------------------------------%
desenhaTabuleiroFinal( Tabuleiro , GoalVal , 1 ):-
	length( Tabuleiro , Tamanho) ,
	NEspacos is (((Tamanho/2)*3)-2) ,
	write('   | ') ,
	desenhaCabecalhoTabuleiro( Tamanho ) , nl ,
	write(' 0 | ') , desenhaGoal( NEspacos , 4 ) , nl , 
	desenhaTabuleiro2( Tabuleiro , 1 ) ,
	N is Tamanho+1 , write(' ') , write(N) , write(' | ') , 
	desenhaGoal( NEspacos , GoalVal ) , nl .
desenhaTabuleiroFinal( Tabuleiro , GoalVal , 2 ):-
	length( Tabuleiro , Tamanho) ,
	NEspacos is (((Tamanho/2)*3)-2) ,
	write('   | ') ,
	desenhaCabecalhoTabuleiro( Tamanho ) , nl ,
	write(' 0 | ') , desenhaGoal( NEspacos , GoalVal ) , nl , 
	desenhaTabuleiro2( Tabuleiro , 1 ) ,
	N is Tamanho+1 , write(' ') , write(N) , write(' | ') , 
	desenhaGoal( NEspacos , 4 ) , nl .

%-----------------------------------------------------------%
% 			Imprime uma linha (valores numa lista)
%-----------------------------------------------------------%
desenhaLinha( [] ) :- nl.
desenhaLinha( [H|T] ) :-
	escreveSimbolo(H) , write('  ') ,
	desenhaLinha(T).

%-----------------------------------------------------------%
% 		  Imprime na consola a numeracao das colunas
%-----------------------------------------------------------%
% -<>- Recebe o numero de colunas a numerar e desenha -<>-  %
desenhaCabecalhoTabuleiro( N ):-
	desenhaCabecalhoTabuleiro( N , 1 ).
	
% -<>-  O ValorColuna indica o numero de uma coluna   -<>-  %
desenhaCabecalhoTabuleiro( 0 , ValorColuna ):-
	nl , write('   --') ,
	for_write( 0 , ValorColuna-1 , 1 , '-' ).

% -<>-      Imprime o numero da coluna e espacos      -<>-  %
% -<>-<>- o numero de espacos difere de acordo com o numero 
% de algarismos para representar um numero 
% -<>-<>-
desenhaCabecalhoTabuleiro( N , ValorColuna ):-
	ValorColuna < 10 ,
	N1 is N - 1 ,
	ValorColuna1 is ValorColuna + 1 ,
	write(ValorColuna) , write(' ') , write(' ') ,
	desenhaCabecalhoTabuleiro( N1 , ValorColuna1 ).

desenhaCabecalhoTabuleiro( N , ValorColuna ):-
	N1 is N - 1 ,
	ValorColuna1 is ValorColuna + 1 ,
	write(ValorColuna) , write(' ') ,
	desenhaCabecalhoTabuleiro( N1 , ValorColuna1 ).

%-----------------------------------------------------------%
%     Ciclo for(int i=I ; i<N ; i=i+P) { printf(C) ; } ;
%-----------------------------------------------------------%
for_write( I , N , P , C ):-
	I < N ,
	I1 is I + P ,
	write(C) , write(C) , write(C) ,
	for_write( I1 , N , P , C ).
for_write( _ , _ , _ , _ ).


%-----------------------------------------------------------%
%     Simbolos utilizados na representacao do tabuleiro
%-----------------------------------------------------------%
escreveSimbolo(4):- write('G').		% representa o "goal"
escreveSimbolo(0):- write('0').		% representa espaco livre
escreveSimbolo(1):- write('1').		% representa peca simples
escreveSimbolo(2):- write('2').		% representa peca dupla
escreveSimbolo(3):- write('3').		% representa peca tripla


%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%
%-%-%-%-%-%-%-%						 CRIA TABULEIRO DE TAMANHO N      		     		%-%-%-%-%-%-%-%
%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%

%-----------------------------------------------------------%
%   Cria lista de Tamanho listas de Colunas posicoes cada
%-----------------------------------------------------------%
criaTabuleiro( 0 , _ , Tabuleiro , TabuleiroFinal):- 
	TabuleiroFinal = Tabuleiro.
criaTabuleiro( Linhas , Colunas , Tabuleiro , TabuleiroFinal ):-
	Linhas1 is Linhas - 1 ,
	adicionaZeros( Colunas , [] , Resultado ) ,
	cria_lista( Resultado , Tabuleiro , NovoTabuleiro ) ,
	criaTabuleiro( Linhas1 , Colunas , NovoTabuleiro , TabuleiroFinal ).

%-----------------------------------------------------------%
%             Cria lista com Tamanho posicoes
%-----------------------------------------------------------%
adicionaZeros( 0 , Lista , Resultado ):- 
	Resultado = Lista .
adicionaZeros( Tamanho , Lista , Resultado):-
	Tamanho1 is Tamanho - 1 ,
	cria_lista(0, Lista, A) , 
	adicionaZeros(  Tamanho1 , A  , Resultado ).


%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%
%-%-%-%-%-%-%-%						 	   MANIPULACAO DE LISTAS      		     		%-%-%-%-%-%-%-%
%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%

%-----------------------------------------------------------%
% 				   Imprime lista de listas
%-----------------------------------------------------------%
imprimeListas([]).
imprimeListas([H|T]) :-
	imprimeLista(H),
	imprimeListas(T).
%-----------------------------------------------------------%
% 						Imprime lista
%-----------------------------------------------------------%
imprimeLista([]) :- nl.
imprimeLista([H|T]) :-
	write(H), write(' ') ,
	imprimeLista(T).

%-----------------------------------------------------------%
% Cria uma lista nova adicionando um Item no inicio da Lista
%-----------------------------------------------------------%
cria_lista(Item, Lista, [Item|Lista]).

%-----------------------------------------------------------%
% 		Inverte a ordem dos elementos de uma lista
%-----------------------------------------------------------%
% -<>-  Lista=[1,2,3], e  X=[3,2,1] ;
%		Lista=[[1,2,3],[4,5,6]], e X=[[6,5,4],[3,2,1]]
% -<>-
inverte_lista([], []) :- !.
inverte_lista( [H|T] , X ):- 
	!,
    inverte_lista(H, NovoH),
    inverte_lista(T, NovoT),
    append(NovoT, [NovoH], X).
inverte_lista(X, X).

%-----------------------------------------------------------%
% 		Inverte a ordem das listas de uma lista
%-----------------------------------------------------------%
% -<>-  Lista=[1,2,3], e  X=[3,2,1] ;
%		Lista=[[1,2,3],[4,5,6]], e X=[[4,5,6],[1,2,3]]
% -<>-
inverte_lista_listas([],[]).
inverte_lista_listas([H|T],ListaInvertida):-  
	inverte_lista_listas(T,Tinvertido),  
	append(Tinvertido,[H],ListaInvertida).
	
%-----------------------------------------------------------%
% 	  Devolve em Elemento, o valor da posicao Indice
%-----------------------------------------------------------%
% -<>- 		 a numero das posicoes comeca em 1 ! 	   -<>- %
% -<>-<>- na lista [a,b,c] , o Elemento na posicao 1 é a;
% o Limite é apenas utilizado como medida de controlo para
% não se exeder o número de elementos da lista
% -<>-<>-
nth_elemento( 1 , _ , [H|_] , Elemento ):-
	Elemento is H.
nth_elemento( Indice , Limite , [_|T] , Elemento ):-
	Indice > 1,
	Indice =< Limite ,
	N is Indice-1 ,
	nth_elemento( N , Limite , T , Elemento ).
	
%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%
%-%-%-%-%-%-%-%						 		MOTOR DE JOGO    		  		     		%-%-%-%-%-%-%-%
%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%-%
inicializa(Variavel):-
	integer(Variavel).
inicializa(Variavel):-
	Variavel is 0 .
%-----------------------------------------------------------------------------------------------------%	
	
play( Tabuleiro , DimensaoTabuleiro):-
	nl , nl , nl , nl , nl ,
	cabecalhoJogo ,
	desenhaTabuleiro1( Tabuleiro , 4 ) ,
	play( Tabuleiro , DimensaoTabuleiro , 1 ) .

play( Tabuleiro , DimensaoTabuleiro , JogadorAtivo ):-
	jogadorAtivo( JogadorAtivo ) ,
	%(% se o movimento for valido, ele é efetuado %)%

	movimenta( JogadorAtivo , Tabuleiro , TabuleiroResultado , DimensaoTabuleiro , Win ) ,
	
	inicializa(Win) ,
	
	%(%  IF  %)%
	( Win=:=0 ->
		desenhaTabuleiro1( TabuleiroResultado , 4 ) ,
		proximoJogador( JogadorAtivo , ProximoJogador ) ,
		play( TabuleiroResultado , DimensaoTabuleiro , ProximoJogador ) ;
	%(% ELSE %)%
	  nl,nl, desenhaTabuleiroFinal( TabuleiroResultado , Win , JogadorAtivo ) ,
	  gameOver( JogadorAtivo )
	).


gameOver( JogadorVencedor ):-
	write('________.: WINNER :.________') , nl ,
	write('Parabens o jogador ') , write(JogadorVencedor) , write(' ganhou!') , nl ,
	write('----------------------------') , nl .
	
	
movimenta( Jogador , Tabuleiro , TabuleiroFinal , DimensaoTab , WinFlag ):-
		%(% escolhe a peca a mover e para onde %)%
		escolhePeca( [PosX,PosY] , [PosFX,PosFY] ) ,
		
		%(% Verifica se as posicoes estao dentro dos limites do tabuleiro %)%
		PosX > 0 , PosFX > 0 , PosY > 0 , 
			PosFY >= 0 , %(% posicao do goal a norte! %)%
		PosX =< DimensaoTab , PosFX =< DimensaoTab , PosY =< DimensaoTab , 
			GoalSul is DimensaoTab+1 , PosFY =< GoalSul ,
		
		%(% Determina a linha jogável de acordo com o jogador %)%
		linhaJogavel( Tabuleiro , DimensaoTab , Jogador , Linha ) ,
		Linha =:= PosY , %--! verifica se o ponto inicial é válido !--%

		%(% Calcula distancia percorrida %)%
		valorAbsoluto( PosFX , PosX , DistX ) ,
		valorAbsoluto( PosFY , PosY , DistY ) ,
		
		DistanciaTotal is  DistX+DistY ,
		
		%(% Pecas do tabuleiro %)%
		getPeca( Tabuleiro , PosX , PosY , PecaInicial ) ,
		(
		%(% IF %)%
			( PosFY =:= 0 ; PosFY =:= GoalSul ) %(% significa que alguem atingiu o 'goal' %)%
				->  setPeca( NovoTabuleiro , PosX , PosY , 0 , TabFinalizado ) ,
					TabuleiroFinal = TabFinalizado , WinFlag=PecaInicial 
			;
			
		%(% ELSE %)%
		(	getPeca( Tabuleiro , PosFX , PosFY , PecaFinal ) ,
			%(% Interpreta tipo de movimento %)%
			%--! movimento simples : DistanciaTotal=:=TipoPeca !--%
			(
				DistanciaTotal =:= PecaInicial ;
				(PecaInicial=:=3 , DistanciaTotal=:=1)
			) ,
			%(% IF %)%
			( PecaFinal=:=0 -> 	setPeca(Tabuleiro, PosFX, PosFY, PecaInicial, NovoTabuleiro) ,
								setPeca(NovoTabuleiro, PosX, PosY, PecaFinal, NovoTabuleiro2) ,
								TabuleiroFinal = NovoTabuleiro2 
			%(% ELSE %)%
			;  ( setPeca(Tabuleiro, PosX, PosY, 0, NovoTabuleiro) , 
				movimentoBonus( NovoTabuleiro , PecaInicial , PecaFinal , [PosFX,PosFY] , Tab , Jogador , Win ) ,
				WinFlag = Win , TabuleiroFinal = Tab
				)
			), !
		)
		).
		
		

movimenta( Jogador , Tabuleiro , TabuleiroFinal , DimensaoTab , WinFlag ):-
	write('Peca invalida. Escolha outra.') , nl ,
	movimenta( Jogador , Tabuleiro , TabuleiroFinal , DimensaoTab , WinFlag ) .


%-----------------------------------------------------------%
% 					Movimentos Bonus
%-----------------------------------------------------------%
movimentoBonus( Tabuleiro , PInicial , PFinal , [X,Y] , NovoTabuleiro , Jogador , Win ):-	
	%(% escolhe tipo de bonus %)%
	! , write('Que tipo de bonus pretende utilizar: (1) Saltar , (2) Trocar') , nl,
	write('Opcao: ') , read(Opcao) , number(Opcao) , Opcao>=1 , Opcao=<2 ,
	%(% IF %)%
	( Opcao=:=2 -> bonusTrocaPecas(Tabuleiro,PInicial,PFinal,[X,Y],NovoTabuleiro,Jogador) , Win=0
	%(% ELSE %)%
	; Opcao=:=1 -> bonusSaltaPeca(Tabuleiro,PInicial,PFinal,[X,Y],Tab,Jogador,Peca) ,
					Win=Peca , NovoTabuleiro=Tab
	).
	
movimentoBonus( Tabuleiro , PInicial , PFinal , [X,Y] , NovoTabuleiro , Jogador , Win ):-	
	write('Opcao invalida. Tente novamente.') , nl ,
	movimentoBonus( Tabuleiro , PInicial , PFinal , [X,Y] , NovoTabuleiro , Jogador , Win ).

writeBonus(N):-
	%(% IF %)%
	(N=:=1 -> nl , nl ,
		write('______.:BONUS:.______') , nl ,
		write('>>> Trocar  pecas <<<') , nl ,
		write('---------------------') , nl ; 
	%(% ELSE %)%
	N=:=2 , nl , nl ,
		write('______.:BONUS:.______') , nl ,
		write('> Saltar  posicaoes <') , nl ,
		write('---------------------') , nl ).
%-----------------------------------------------------------%
% 						Bonus Troca
%-----------------------------------------------------------%
bonusTrocaPecas([H|T],PInicial,PFinal,[X,Y],NovoTabuleiro,Jogador):-
	writeBonus(1), length(H,DimensaoTabuleiro) ,
	%(% faz leitura da nova posicao e valida : dentro do tabuleiro %)%
	write('Nova posicao para a peca ') , write(PFinal) , nl ,
	write('>>> x : ') , read(NovoX) , number(NovoX) , NovoX>0 , NovoX=<DimensaoTabuleiro ,
	write('>>> y : ') , read(NovoY) , number(NovoY) , NovoY>0 , NovoY=<DimensaoTabuleiro ,
	
	%(% a peca nova nao pode ser colocada atras da linha jogavel do adversario %)%
	% IF jogador a norte : nao pode por abaixo da linha jogavel a sul
	( Jogador=:=1 -> linhaJogavel( [H|T] , DimensaoTabuleiro , 2 , Linha2 ) ,
					 NovoY =< Linha2 ;
	% ELSE jogador a sul : nao pode por acima da linha jogavel a norte
	   Jogador=:=2 , linhaJogavel( [H|T] , DimensaoTabuleiro , 1 , Linhal ) ,
					 NovoY >= Linhal ) ,
	
	%(% verificar se a nova posicao está livre %)%
	getPeca( [H|T] , NovoX , NovoY , Posicao ) ,
		% IF livre
		( Posicao=:=0 -> 	setPeca([H|T], X, Y, PInicial, NovoTabuleiro1) ,
							setPeca(NovoTabuleiro1, NovoX, NovoY, PFinal, NovoTabuleiro2) , ! ,
							NovoTabuleiro = NovoTabuleiro2 
		% ELSE
		; 	write('A posicao escolhida encontra-se ocupada. Escolha outra.') , nl ,
			bonusTrocaPecas([H|T],PInicial,PFinal,[X,Y],NovoTabuleiro,Jogador)).
			
bonusTrocaPecas([H|T],PInicial,PFinal,[X,Y],NovoTabuleiro,Jogador):-
	write('Posicao invalida. Tente novamente.') , nl ,
	bonusTrocaPecas([H|T],PInicial,PFinal,[X,Y],NovoTabuleiro,Jogador).
	
%-----------------------------------------------------------%
% 						Bonus Salta
%-----------------------------------------------------------%
bonusSaltaPeca([H|T],PInicial,PFinal,[X,Y],NovoTabuleiro,Jogador,Win):- 
	writeBonus(2) , length(H,DimensaoTabuleiro) , TamanhoSalto=PFinal ,
	write('Para onde pretende mover a peca ') , write(PInicial) , nl ,
	write('>>> x : ') , read(NovoX) , number(NovoX) , NovoX>0 , NovoX=<DimensaoTabuleiro ,
	write('>>> y : ') , read(NovoY) , number(NovoY) , NovoY>=0 , N is DimensaoTabuleiro+1,  NovoY=<N ,
	
	%(% Calcula distancia percorrida %)%
		valorAbsoluto( NovoX , X , DistX ) ,
		valorAbsoluto( NovoY , Y , DistY ) ,
		DistanciaTotal is  DistX+DistY ,
		
	%(% Interpreta tipo de movimento %)%
		%--! movimento simples : DistanciaTotal=:=TipoPeca !--%
		(DistanciaTotal =:= TamanhoSalto ;
		(PFinal=:=3 , DistanciaTotal=:=1)) ,
		
	%(% IF %)%
	( ( NovoY =:= 0 ; NovoY =:= N ) 
		-> Win is PInicial , NovoTabuleiro=[H|T] , ! 
	; 
	%(% ELSE %)%
		%(% verificar se a nova posicao está livre %)%
		getPeca( [H|T] , NovoX , NovoY , Posicao ) ,
			% IF livre
			( Posicao=:=0 -> 	setPeca([H|T], NovoX, NovoY, PInicial, NovoTabuleiro1) ,
								NovoTabuleiro = NovoTabuleiro1 
			% ELSE
			; 	write('Ja existe um peca nesse local Ganhou um novo Bonus.') , nl ,
				movimentoBonus( [H|T] , PInicial , Posicao , [NovoX,NovoY] , NovoTabuleiro , Jogador , Win ))
	).
	
	

bonusSaltaPeca(Tabuleiro,PInicial,PFinal,[X,Y],NovoTabuleiro,Jogador,Win):-
	write('Posicao invalida. Apenas pode deslocar-se ') , write(PFinal) , write(' posicoes.') , nl ,
	write('Tente novamente.') , nl ,
	bonusSaltaPeca(Tabuleiro,PInicial,PFinal,[X,Y],NovoTabuleiro,Jogador,Win).
	
%-----------------------------------------------------------%
% 						Get Peca
%-----------------------------------------------------------%	
% -<>- Devolve o valor da peca presente no Tabuleiro na 
% posicao ( X , Y ) 
% -<>-
getPeca( Tabuleiro , X , Y , Peca ):-
	procuraLinha( Tabuleiro , Y , 1, LinhaObtida ) ,
	length( LinhaObtida , N ) ,
	nth_elemento( X , N , LinhaObtida , Elemento ) ,
	Peca = Elemento .

getPeca2( Tabuleiro , X , Y , Peca ):-
	procuraLinha( Tabuleiro , Y , 1, LinhaObtida ) ,
	length( LinhaObtida , N ) ,
	nth_elemento( X , N , LinhaObtida , Elemento ) ,
	Peca = Elemento .
getPeca2( _ , _ , _ , -1 ):-!.
%-----------------------------------------------------------%
% 						Set Peca
%-----------------------------------------------------------%	
setPeca( Tabuleiro , X , Y , Peca , NovoTabuleiro ):-
	constroiTabuleiro( Tabuleiro , Y , 1 , [] , TabuleiroParte1 ) ,
	procuraLinha( Tabuleiro , Y , 1 , LinhaObtida ) ,
	alteraLista( LinhaObtida , X , Peca , ListaAlterada , [] ) ,
	append( TabuleiroParte1 , [ListaAlterada] , TabuleiroParte2 ) ,
	inverte_lista_listas( Tabuleiro , TabuleiroInvertido ) ,
	length(Tabuleiro , N ) , Y1 is N-(Y-1) ,
	constroiTabuleiro( TabuleiroInvertido , Y1 , 1 , [] , TabuleiroParte3 ) ,
	inverte_lista_listas( TabuleiroParte3 , TabuleiroParte31 ) ,
	append( TabuleiroParte2 , TabuleiroParte31 , TabuleiroFinal ) ,
	NovoTabuleiro = TabuleiroFinal .	
%-----------------------------------------------------------%
% 						Set e Get Auxiliar
%-----------------------------------------------------------%
constroiTabuleiro( [H|T] , NLinha , IntI , LAux , TabuleiroParte1 ):-
	NLinha > IntI , 
	I is IntI + 1 ,
	cria_lista( H , LAux , NovoTab ) ,
	constroiTabuleiro( T , NLinha , I , NovoTab, TabuleiroParte1 ) .
constroiTabuleiro( _ , _ , _ , LAux , TabuleiroParte1 ):-
	inverte_lista_listas( LAux , Nova ) ,
	TabuleiroParte1 = Nova .
procuraLinha( [_|T] , NLinha , IntI , LinhaResultado ):-
	NLinha > IntI , 
	I is IntI + 1 ,
	procuraLinha( T , NLinha , I , LinhaResultado ).
procuraLinha( [H|_] , _ , _ , LinhaResultado ):-
	LinhaResultado = H .

%-----------------------------------------------------------%
% 						Valor absoluto
%-----------------------------------------------------------%
% -<>- Faz a diferenca entre o Valor1 e o Valor2 e devolve em 
% ValorAbsoluto o valor absoluto resultante
% -<>-
valorAbsoluto( Valor1 , Valor2 , ValorAbsoluto ):-
	Diferenca is Valor1-Valor2 ,
	Diferenca>=0 , ! , ValorAbsoluto=Diferenca.
valorAbsoluto( Valor1 , Valor2 , ValorAbsoluto ):-
	Diferenca is Valor2-Valor1 ,
	Diferenca>=0 , ! , ValorAbsoluto=Diferenca.

%-----------------------------------------------------------%
% 					  Escolher Pecas
%-----------------------------------------------------------%
% -<>- Este predicado apenas faz a leitura de 4 inputs validos
% dados pelo jogador, a verificação de validade e relevância
% para o tabuleiro de jogo é feita depois no decorrer
% do predicado 'movimenta'
% -<>-
escolhePeca( [P1,P2] , [P3,P4] ):-
	write('Posicao da peca o movimentar: ') , nl ,
	write('>>>  x = ') , read(P1) , number(P1) ,
	write('>>>  y = ') , read(P2) , number(P2) ,
	write('Posicao final da peca anterior: ') , nl ,
	write('>>>  x = ') , read(P3) , number(P3) ,
	write('>>>  y = ') , read(P4) , number(P4) , nl .
	
%-----------------------------------------------------------%
% 					Imprime Jogador Ativo
%-----------------------------------------------------------%
% -<>- Imprime no ecra de forma formatada o jogador atual -<>- %
jogadorAtivo( Jogador ):-
		Jogador =:= 0 , ! ,
		write('__________________________________') , nl ,
		write('>>> Jogador ativo : Computador <<<') , nl ,
		write('__________________________________') , nl , nl .
jogadorAtivo( Jogador ):-
		write('_________________________') , nl ,
		write('>>> Jogador ativo : ') , write(Jogador) , write(' <<<') , nl ,
		write('_________________________') , nl , nl .

%-----------------------------------------------------------%
% 				Calcula o próximo Jogador
%-----------------------------------------------------------%		
% -<>- Calcula o proximo jogador de acordo com o atual -<>- %
proximoJogador( Jogador , ProximoJogador ):-
	Jogador =:= 1 , ProximoJogador is 2 .	
proximoJogador( Jogador , ProximoJogador ):-
	Jogador =:= 2 , ProximoJogador is 1 .

%-----------------------------------------------------------%
% 						Linha Jogável
%-----------------------------------------------------------%	
% -<>- Calcula numa lista de listas, qual a primeira com
% para jogar pelo jogador ativo 
% -<>-<>- LinhaJogavel é a posição+1 na lista de listas
% -<>-
linhaJogavel( Tabuleiro , _ , 1 , LinhaJogavel ):-
	analisa( Tabuleiro , 1 , Resultado) , ! ,
	LinhaJogavel is Resultado.	
linhaJogavel( Tabuleiro , DimensaoTabuleiro , 2 , LinhaJogavel ):-
	inverte_lista_listas( Tabuleiro , Tab1 ) , 
	analisa( Tab1 , 1 , Resultado ) , ! ,
	LinhaJogavel is (DimensaoTabuleiro+1)-Resultado .
	
% -<>- O predicado analisa recebe uma lista de listas e
% faz uma analise, retornando a primeira linha jogável
% de acordo com a posição do jogador em relação ao tabuleiro
% -<>-
analisa( [[]]  , LinhaJogavel , Resultado ):- Resultado is LinhaJogavel .
analisa( [H|T] , LinhaJogavel , Resultado ):-
	empty( H , Flag ) ,
	Flag =:= 1 , Linha is LinhaJogavel + 1 ,
	analisa( T , Linha , Resultado ).
analisa( _     , LinhaJogavel , Resultado ):- Resultado is LinhaJogavel .

% -<>- Retorna TRUE (1) ou FALSE (0) caso a lista seja
% vazia ou não, isto é se só tem zeros ou não
% -<>-
empty( [] , 1 ). %(% TRUE %)%
empty( [H|T] , Result ):- zero( H , _ ) , ! , empty( T , Result ) .
empty( _ , 0 ). %(% FAL SE %)%
zero( N , R ):- N =:= 0 , ! , R is N .


%-----------------------------------------------------------%
% 						Jogada valida
%-----------------------------------------------------------%	
% -<>- Calcula se apartir de uma posicao X,Y é possivel chegar a PosX PosY 
% em exatamente TipoPeca(1,2,3) jogadas num tabuleiro Tab de tamanho N, sem
% passar por outras pecas ou visitando uma posicao mais do que 1 vez.
% Nao contabiliza jogadas bonus, do tipo salta ou troca.
	
%Valid and won!
jogadaValida( Tabuleiro , TamanhoTabuleiro , [Xi,Yi] , [Xf,Yf] , TipoPeca , Jogador , Resultado ):-
	linhaJogavel( Tabuleiro , TamanhoTabuleiro , Jogador , Linha ) ,
	Linha =:= Yi , ! ,
	jogadaValidaAux( Tabuleiro , TamanhoTabuleiro , [Xi,Yi] , [Xf,Yf] , TipoPeca , Jogador , ResultadoValida ) ,
	Resultado = ResultadoValida.


jogadaValidaAux(_,_,[_,1],[_,0],1,2,1).

jogadaValidaAux(_,N,[_,Y],[_,Z],1,1,2):-
	Y=:=N,
	Z=:=N+1.
jogadaValidaAux(Tab,_,[X,Y],[PosX,PosY],1,1,Result):-
	PosY=\=0,
	getPeca(Tab,PosX,PosY,Test),
	getPeca(Tab,X,Y,Test1),
	Xm is X-1,
	XM is X+1,
	Ym is Y-1,
	YM is Y+1,
	((X=:=PosX,(Ym=:=PosY;YM=:=PosY));
	(Y=:=PosY,(Xm=:=PosX;XM=:=PosX))),
	write('Origin: '), write(X), write(','),write(Y),write(' -> '),write(Test1),nl,
	write('Destiny: '), write(PosX), write(','),write(PosY),write(' -> '),write(Test),nl,
	write('Valido mas nao ganhou'),nl,
	Result is 0.

jogadaValidaAux(Tab,N,[X,Y],[PosX,PosY],1,2,Result):-
	PosY=\=N+1,
	getPeca(Tab,PosX,PosY,Test),
	getPeca(Tab,X,Y,Test1),
	Xm is X-1,
	XM is X+1,
	Ym is Y-1,
	YM is Y+1,
	((X=:=PosX,(Ym=:=PosY;YM=:=PosY));
	(Y=:=PosY,(Xm=:=PosX;XM=:=PosX))),
	write('Origin: '), write(X), write(','),write(Y),write(' -> '),write(Test1),nl,
	write('Destiny: '), write(PosX), write(','),write(PosY),write(' -> '),write(Test),nl,
	write('Valido mas nao ganhou'),nl,
	Result is 0.
	
jogadaValidaAux(Tab,N,[X,Y],[PosX,PosY],TipoPeca,Jog,Result):-
	%getPeca( Tab , X , Y , Test ),
	%write('testing position: '), write(X), write(','),write(Y),write(' has: '),write(Test),write(' with: '),write(TipoPeca),write(' jogadas'),nl,
	%Invalidates current position(so no backtracking)
	setPeca(Tab,X,Y,-1,NTab), 
	Aux is TipoPeca-1,Aux>0,!,
	((Xm is X-1, Xm >= 1,(getPeca( NTab , Xm , Y , Teste4 ), Teste4=:=0,Y=\=N+1,Y=\=0,jogadaValidaAux(NTab,N,[Xm,Y],[PosX,PosY],Aux,Jog,Result),write('fui oeste '),nl),!);
	
	(XM is X+1, XM =<N,(getPeca( NTab , XM , Y , Teste2 ), Teste2=:=0 ,(Jog=:=2;Y=\=N+1),Y=\=0,jogadaValidaAux(NTab,N,[XM,Y],[PosX,PosY],Aux,Jog,Result),write('fui este'),nl),!);

	(Ym is Y-1, Ym >= 1,(getPeca( NTab , X , Ym , Teste3 ), Teste3=:=0 ,(Jog=:=2;Y=\=N+1),Y=\=0,jogadaValidaAux(NTab,N,[X,Ym],[PosX,PosY],Aux,Jog,Result),write('fui norte'),nl),!);

	(YM is Y+1, YM =< N,(getPeca( NTab , X , YM , Teste1 ), Teste1=:=0 ,Y=\=N+1,Y=\=0,jogadaValidaAux(NTab,N,[X,YM],[PosX,PosY],Aux,Jog,Result),write('fui sul'),nl),!)).
	
jogadaValidaAux(_,_,[_,_],[_,_],0,_,Answer):-Answer is -1.


bonusTrocaPecas([H|T],[NovoX,NovoY],1,Answer):-
	length(H,DimensaoTabuleiro) ,
	%(% faz leitura da nova posicao e valida : dentro do tabuleiro %)%
	number(NovoX) , NovoX>0 , NovoX=<DimensaoTabuleiro ,
	number(NovoY) , NovoY>0 , NovoY=<DimensaoTabuleiro ,
	
	%(% a peca nova nao pode ser colocada atras da linha jogavel do adversario %)%
	% jogador a norte : nao pode por abaixo da linha jogavel a sul
	linhaJogavel( [H|T] , DimensaoTabuleiro , 2 , Linha ) ,
	NovoY =< Linha , ! ,
	
	%(% verificar se a nova posicao está livre %)%
	getPeca( [H|T] , NovoX , NovoY , Posicao ) ,
	(Posicao =:= 0 -> Answer is 1 ;
					  Answer is -1 ).

bonusTrocaPecas([H|T],[NovoX,NovoY],2,Answer):-
	length(H,DimensaoTabuleiro) ,
	%(% faz leitura da nova posicao e valida : dentro do tabuleiro %)%
	number(NovoX) , NovoX>0 , NovoX=<DimensaoTabuleiro ,
	number(NovoY) , NovoY>0 , NovoY=<DimensaoTabuleiro ,
	
	%(% a peca nova nao pode ser colocada atras da linha jogavel do adversario %)%
	% jogador a sul : nao pode por acima da linha jogavel a norte
	linhaJogavel( [H|T] , DimensaoTabuleiro , 1 , Linha ) ,
	NovoY >= Linha , ! ,
	
	%(% verificar se a nova posicao está livre %)%
	getPeca( [H|T] , NovoX , NovoY , Posicao ) ,
	(Posicao =:= 0 -> Answer is 1 ;
					  Answer is -1 ).
	
%=============================================================================================%
validaFimJogo( [H|_] , _ , Yi , _ , Yf , 1 , Resultado , Jogador ):-
	length(H , TamanhoTabuleiro) , N is TamanhoTabuleiro+1 ,
	( (Yf =:= 0 , Jogador=:=2) ; (Yf =:= N , Jogador=:=1) ) ,
	valorAbsoluto( Yi , Yf , Dist ) ,
	Dist =:= 1 ,
	Resultado is 1 .
validaFimJogo( _ , _ , _ , _ , _ , 1 , -1 , _ ):-!.



validaFimJogo( [H|T] , Xi , Yi , _ , Yf , 2 , Resultado , Jogador ):-
	length(H , TamanhoTabuleiro) , N is TamanhoTabuleiro+1 ,
	( (Yf =:= 0 , Jogador=:=2) ; (Yf =:= N , Jogador=:=1) ) ,
	valorAbsoluto( Yi , Yf , Dist ) ,
	Dist =:= 2 ,
	Cond is TamanhoTabuleiro -1 ,
	( Yi =:= Cond -> IncY is Yi+1 ; IncY is Yi-1 ) ,
	getPeca2( [H|T] , Xi , IncY , Peca1 ) ,
	(Peca1=:=0 -> Resultado is 1 ; Resultado is -1 ).

validaFimJogo( [H|T] , Xi , Yi , _ , Yf , 2 , Resultado , Jogador ):-
	length(H , TamanhoTabuleiro) , N is TamanhoTabuleiro+1 ,
	( (Yf =:= 0 , Jogador=:=2) ; (Yf =:= N , Jogador=:=1) ) ,
	valorAbsoluto( Yi , Yf , Dist ) ,
	Dist =:= 1 ,
	PosX1 is Xi +1 , PosX2 is Xi -1 ,
	getPeca2( [H|T] , PosX1 , Yi , Peca1 ) , ! ,
	getPeca2( [H|T] , PosX2 , Yi , Peca2 ) , ! ,
	( ( Peca1 =:= 0 ; Peca2 =:= 0 ) -> Resultado is 1 ; Resultado is -1 ).
	
validaFimJogo( _ , _ , _ , _ , _ , 2 , -1 , _ ):-!.



validaFimJogo( [H|T] , Xi , Yi , _ , Yf , 3 , Resultado , Jogador ):-
	length(H , TamanhoTabuleiro) , N is TamanhoTabuleiro+1 ,
	( (Yf =:= 0 , Jogador=:=2) ; (Yf =:= N , Jogador=:=1) ) ,
	valorAbsoluto( Yi , Yf , Dist ) ,
	Dist =:= 3 ,
	Cond is TamanhoTabuleiro -2 ,
	( Yi =:= Cond -> IncY1 is Yi+1 , IncY2 is Yi+2 
					; IncY1 is Yi-1 , IncY2 is Yi-2  ) ,
	getPeca2( [H|T] , Xi , IncY1 , Peca11 ), ! ,
	getPeca2( [H|T] , Xi , IncY2 , Peca12 ), ! ,
	( ( Peca11=:=0 , Peca12=:=0 ) -> Resultado is 1 ; Resultado is -1 ).
	
validaFimJogo( [H|T] , Xi , Yi , _ , Yf , 3 , Resultado , Jogador ):-
	length(H , TamanhoTabuleiro) , N is TamanhoTabuleiro+1 ,
	( (Yf =:= 0 , Jogador=:=2) ; (Yf =:= N , Jogador=:=1) ) ,
	valorAbsoluto( Yi , Yf , Dist ) ,
	Dist =:= 2 ,
	IncX is Xi +1 , DecX is Xi -1 ,
	Cond is TamanhoTabuleiro -1 ,
	( Yi =:= Cond -> NewY is Yi +1 ; NewY is Yi -1 ) ,
	% caminho 1
	getPeca2( [H|T] , IncX , Yi , Peca11 ), ! ,
	getPeca2( [H|T] , IncX , NewY , Peca12 ), ! ,
	% caminho 2
	getPeca2( [H|T] , DecX , Yi , Peca21 ), !, 
	getPeca2( [H|T] , DecX , NewY , Peca22 ), !,
	% caminho 3
	getPeca2( [H|T] , Xi , NewY , Peca31 ), !,
	getPeca2( [H|T] , IncX , NewY , Peca32 ), !,
	% caminho 4
	getPeca2( [H|T] , Xi , NewY , Peca41 ), !,
	getPeca2( [H|T] , DecX , NewY , Peca42 ), !,
	(
		( (Peca11=:=0,Peca12=:=0) ; (Peca21=:=0,Peca22=:=0) ;
		(Peca31=:=0,Peca32=:=0) ; (Peca41=:=0,Peca42=:=0) )
			-> Resultado is 1 ; Resultado is -1 
	).

validaFimJogo( [H|T] , Xi , Yi , _ , Yf , 3 , Resultado , Jogador ):-
	length(H , TamanhoTabuleiro) , N is TamanhoTabuleiro+1 ,
	( (Yf =:= 0 , Jogador=:=2) ; (Yf =:= N , Jogador=:=1) ) ,
	valorAbsoluto( Yi , Yf , Dist ) ,
	Dist =:= 1 ,
	IncX1 is Xi +1 , DecX1 is Xi -1 , IncX2 is Xi +2 , DecX2 is Xi -2 ,
	% caminho 1
	getPeca2( [H|T] , IncX1 , Yi , Peca11 ) , !, 
	getPeca2( [H|T] , IncX2 , Yi , Peca12 ) , ! ,
	% caminho 2
	getPeca2( [H|T] , DecX1 , Yi , Peca21 ) , !, 
	getPeca2( [H|T] , DecX2 , Yi , Peca22 ) , !,
	( ( (Peca11=:=0,Peca12=:=0) ; (Peca21=:=0,Peca22=:=0) )
	-> Resultado is 1 ; Resultado is -1 ).
	
validaFimJogo( _ , _ , _ , _ , _ , 3 , -1 , _ ):- !.