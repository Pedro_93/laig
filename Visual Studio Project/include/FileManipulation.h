#ifndef FILEMANIP_H
#define FILEMANIP_H

#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <map>

using namespace std ;

typedef multimap <double,string, less<double> > HighScore ;

const string HSFilename = "../data/highscores.txt" ;

inline void verify(HighScore * hs)
{
    if ( hs->size() > 10 )
    {
        HighScore::iterator it = hs->begin() ;
        std::advance(it , 10) ;
        hs->erase( it , hs->end() ) ;
    }
}

inline void writeHighScores( string filename , HighScore * hs )
{
    verify( hs ) ;
	ofstream writer( filename.c_str() , ios::out ) ;

	if( !writer )
	{
		cerr << "[WRITE] Nao foi possivel abrir o ficheiro de pontuacoes!" << endl ;
		getchar() ;
		return ;
	}

	for( HighScore::const_iterator it = hs->begin() ; it != hs->end() ; it++ )
    {
        writer << it->first << ' ' << it->second << endl ;
    }

	writer.close() ;
}

inline HighScore * readHighScores( string filename )
{
	ifstream reader( filename.c_str() , ios::in ) ;

	if( !reader )
	{
		cerr << "[READ] Nao foi possivel abrir o ficheiro de pontuacoes!" << endl ;
		getchar() ;
	}

	string playerName ;
	double playerTime ;
	HighScore * recordsInFile = new HighScore;

	while( reader >> playerTime >> playerName )
		recordsInFile->insert( HighScore::value_type( playerTime , playerName ) ) ;

	reader.close() ;

    verify( recordsInFile ) ;
	return recordsInFile ;
}

inline void creatTextFile( string filename )
{
    ifstream infile(filename.c_str());
    if( infile )
        return ;
    else
    {
        ofstream textFile;
        textFile.open(filename.c_str());
        textFile.close();
    }
}

inline void addHighScore( HighScore * hs , string player , double score )
{
    hs->insert( HighScore::value_type( score , player ) ) ;
    verify( hs ) ;
}

inline bool addInTopTen( HighScore * hs , double score )
{
    if ( hs->size()==0 )
        return true ;

    HighScore::const_iterator it = hs->end() ;
    it-- ;

    if( score <= it->first )
        return true ;
    else
        return false ;
}
#endif
