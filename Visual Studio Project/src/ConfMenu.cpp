#include "ConfMenu.h"
#include <iostream>

ConfMenu::ConfMenu(int op1 , int op2 , int op3 , int op4)
{
	FLAG_APPCHANGE = false ;

	GameType=op1;
	Dificulty=op2;
	BoardDraw=op3;
	Background=op4;
	setOption1(op1);
	setOption2(op2);
	setOption3(op3);
	setOption4(op4);

	this->board = new UnitCube() ;
	this->back_tab = new CGFappearance("../data/game_menu/back.jpg",1.0,1.0) ;
	this->conf_tab = new CGFappearance("../data/game_menu/config_menu.jpg",1.0,1.0) ;
	this->background = new CGFappearance("../data/game_menu/background.jpg",15.0,8.0) ;
	this->symbol = new CGFappearance("../data/game_menu/symbol.jpg",1.0,1.0) ;
	this->checked = new CGFappearance("../data/game_menu/checked.jpg",1.0,1.0) ;
	this->unchecked = new CGFappearance("../data/game_menu/unchecked.jpg",1.0,1.0) ;
	this->characters = new CGFappearance("../data/solid/white.jpg",1.0,1.0) ;
}

ConfMenu::~ConfMenu()
{
	delete(board) ;
	delete(back_tab) ;
	delete(conf_tab) ;
	delete(background) ;
	delete(symbol) ;
}

void ConfMenu::init()
{
	initialCamera = new ortho("otho",0.0,100.0,-25.0,25.0,25.0,-25.0) ;
	setOption1(Dificulty);
	setOption2(GameType);
	setOption3(BoardDraw);
	setOption4(Background);
};
void ConfMenu::leave(){};

void ConfMenu::display()
{
	initialCamera->applyView();
	glPushMatrix() ;
	glTranslatef(0,-1,0);
	this->drawDifficulty() ;
	glPopMatrix() ;
	glPushMatrix() ;
	glTranslatef(0,-4,0) ;
	this->drawGameMode() ;
	glPopMatrix() ;
	glPushMatrix() ;
	glTranslatef(0,-7,0) ;
	this->drawBoardMode() ;
	glPopMatrix() ;
	glPushMatrix() ;
	glTranslatef(0,-10,0) ;
	this->drawBackGroundMode() ;
	glPopMatrix() ;

	//conf
	glPushMatrix() ;
	conf_tab->apply() ;
	glTranslatef(-6.5,4.5,0.55) ;
	glScalef(8,2.0,0.1) ;
	board->drawface() ;
	glPopMatrix() ;

	// back tab
	glPushMatrix() ;
	glPushName(0) ;
	back_tab->apply();
	glTranslated(-6.5,-6.0,0.55) ;
	glScalef(4,1.0,0.1) ;
	board->drawface() ;
	glPopName() ;
	glPopMatrix() ;

	// symbol
	glPushMatrix() ;
	symbol->apply() ;
	glTranslatef(-6.5,-1,0.1);
	glScalef(6.0,6.0,0.1) ;
	board->draw() ;
	glPopMatrix() ;

	// background
	glPushMatrix() ;
	background->apply() ;
	glScalef(25.0,25.0,0.1) ;
	board->draw() ;
	glPopMatrix() ;
}
void ConfMenu::drawDifficulty() // op1
{
	glPushMatrix() ;
	glTranslatef(2,3,0) ;
	glPushMatrix() ;
	characters->apply() ;
	glTranslated(-4,1.5,0.55) ;
	glScalef(0.005, 0.005, 0.005);
	glColor3f(1.0,1.0,1.0);
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'D' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'i' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'f' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'f' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'i' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'c' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'u' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'l' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 't' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'y' ) ;
	glPopMatrix() ;
	glPushMatrix() ;
	characters->apply() ;
	glTranslated(-2,0.5,0.55) ;
	glScalef(0.005, 0.005, 0.005);
	glColor3f(1.0,1.0,1.0);
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'e' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'a' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 's' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'y' ) ;
	glPopMatrix() ;
	glPushMatrix() ;
	characters->apply() ;
	glTranslated(1,0.5,0.55) ;
	glScalef(0.005, 0.005, 0.005);
	glColor3f(1.0,1.0,1.0);
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'm' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'e' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'd' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'i' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'u' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'm' ) ;
	glPopMatrix() ;
	glPushMatrix() ;
	characters->apply() ;
	glTranslated(5.5,0.5,0.55) ;
	glScalef(0.005, 0.005, 0.005);
	glColor3f(1.0,1.0,1.0);
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'h' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'a' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'r' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'd' ) ;
	glPopMatrix() ;

	// check boxes
	glPushMatrix() ;
	glPushName(DIFF_EASY) ; // easy
	glTranslatef(0,0.65,0.1) ;
	if( option1 == DIFF_EASY ) this->drawCheckBox(BOX_CHECK) ;
	else this->drawCheckBox(BOX_UNCHECK) ;
	glPopName() ;
	glPopMatrix() ;
	glPushMatrix() ;
	glPushName(DIFF_MEDIUM) ; // medium
	glTranslatef(4,0.65,0.1) ;
	if( option1 == DIFF_MEDIUM ) this->drawCheckBox(BOX_CHECK) ;
	else this->drawCheckBox(BOX_UNCHECK) ;
	glPopName() ;
	glPopMatrix() ;
	glPushMatrix() ;
	glPushName(DIFF_HARD) ; // hard
	glTranslatef(7.8,0.65,0.1) ;
	if( option1 == DIFF_HARD ) this->drawCheckBox(BOX_CHECK) ;
	else this->drawCheckBox(BOX_UNCHECK) ;
	glPopName() ;
	glPopMatrix() ;
	glPopMatrix() ;
}
void ConfMenu::drawGameMode() // op2
{
	glPushMatrix() ;
	glTranslatef(2,3,0) ;
	glPushMatrix() ;
	characters->apply() ;
	glTranslated(-4,1.5,0.55) ;
	glScalef(0.005, 0.005, 0.005);
	glColor3f(1.0,1.0,1.0);
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'G' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'a' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'm' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'e' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , ' ' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'M' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'o' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'd' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'e' ) ;
	glPopMatrix() ;
	glPushMatrix() ;
	characters->apply() ;
	glTranslated(-2,0.5,0.55) ;
	glScalef(0.005, 0.005, 0.005);
	glColor3f(1.0,1.0,1.0);
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'P' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'v' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 's' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'P' ) ;
	glPopMatrix() ;
	glPushMatrix() ;
	characters->apply() ;
	glTranslated(1.25,0.5,0.55) ;
	glScalef(0.005, 0.005, 0.005);
	glColor3f(1.0,1.0,1.0);
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'P' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'v' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 's' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'P' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'C' ) ;
	glPopMatrix() ;
	glPushMatrix() ;
	characters->apply() ;
	glTranslated(5,0.5,0.55) ;
	glScalef(0.005, 0.005, 0.005);
	glColor3f(1.0,1.0,1.0);
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'P' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'C' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'v' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 's' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'P' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'C' ) ;
	glPopMatrix() ;

	// check boxes
	glPushMatrix() ;
	glPushName(MODE_PvP) ; // p vs p
	glTranslatef(0,0.75,0.1) ;
	if( option2 == MODE_PvP ) this->drawCheckBox(BOX_CHECK) ;
	else this->drawCheckBox(BOX_UNCHECK) ;
	glPopName() ;
	glPopMatrix() ;
	glPushMatrix() ;
	glPushName(MODE_PvPC) ; // p vs pc
	glTranslatef(4,0.75,0.1) ;
	if( option2 == MODE_PvPC ) this->drawCheckBox(BOX_CHECK) ;
	else this->drawCheckBox(BOX_UNCHECK) ;
	glPopName() ;
	glPopMatrix() ;
	glPushMatrix() ;
	glPushName(MODE_PCvPC) ; // pc vs pc
	glTranslatef(7.8,0.75,0.1) ;
	if( option2 == MODE_PCvPC ) this->drawCheckBox(BOX_CHECK) ;
	else this->drawCheckBox(BOX_UNCHECK) ;
	glPopName() ;
	glPopMatrix() ;
	glPopMatrix() ;
}
void ConfMenu::drawBoardMode() // op3
{
	glPushMatrix() ;
	glTranslatef(2,3,0) ;
	glPushMatrix() ;
	characters->apply() ;
	glTranslated(-4,1.5,0.55) ;
	glScalef(0.005, 0.005, 0.005);
	glColor3f(1.0,1.0,1.0);
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'B' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'o' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'a' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'r' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'd' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , ' ' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'D' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'r' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'a' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'w' ) ;
	glPopMatrix() ;
	glPushMatrix() ;
	characters->apply() ;
	glTranslated(-2,0.5,0.55) ;
	glScalef(0.005, 0.005, 0.005);
	glColor3f(1.0,1.0,1.0);
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'r' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'e' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'c' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 't' ) ;
	glPopMatrix() ;
	glPushMatrix() ;
	characters->apply() ;
	glTranslated(1,0.5,0.55) ;
	glScalef(0.005, 0.005, 0.005);
	glColor3f(1.0,1.0,1.0);
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'r' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'o' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'u' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'n' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'd' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'e' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'd' ) ;
	glPopMatrix() ;

	// check boxes
	glPushMatrix() ;
	glPushName(BORDER_RECT) ; // rect
	glTranslatef(0,0.85,0.1) ;
	if( option3 == BORDER_RECT ) this->drawCheckBox(BOX_CHECK) ;
	else this->drawCheckBox(BOX_UNCHECK) ;
	glPopMatrix() ;
	glPushMatrix() ;
	glPopName() ;
	glPushName(BORDER_ROUNDED) ; // rounded
	glTranslatef(4,0.85,0.1) ;
	if( option3 == BORDER_ROUNDED ) this->drawCheckBox(BOX_CHECK) ;
	else this->drawCheckBox(BOX_UNCHECK) ;
	glPopName() ;
	glPopMatrix() ;
	glPopMatrix() ;
}
void ConfMenu::drawBackGroundMode() // op4
{
	glPushMatrix() ;
	glTranslatef(2,3,0) ;
	glPushMatrix() ;
	characters->apply() ;
	glTranslated(-4,1.5,0.55) ;
	glScalef(0.005, 0.005, 0.005);
	glColor3f(1.0,1.0,1.0);
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'B' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'a' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'c' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'k' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'g' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'r' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'o' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'u' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'n' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'd' ) ;
	glPopMatrix() ;
	glPushMatrix() ;
	characters->apply() ;
	glTranslated(-2,0.5,0.55) ;
	glScalef(0.005, 0.005, 0.005);
	glColor3f(1.0,1.0,1.0);
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'w' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'o' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'o' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'd' ) ;
	glPopMatrix() ;
	glPushMatrix() ;
	characters->apply() ;
	glTranslated(1.5,0.5,0.55) ;
	glScalef(0.005, 0.005, 0.005);
	glColor3f(1.0,1.0,1.0);
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'w' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'a' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 't' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'e' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'r' ) ;
	glPopMatrix() ;
	glPushMatrix() ;
	characters->apply() ;
	glTranslated(5,0.5,0.55) ;
	glScalef(0.005, 0.005, 0.005);
	glColor3f(1.0,1.0,1.0);
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'C' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'o' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'l' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'o' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'r' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'f' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'u' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'l' ) ;
	glPopMatrix() ;

	// check boxes
	glPushMatrix() ;
	glPushName(BG_WOOD) ; // wood
	glTranslatef(0,0.95,0.1) ;
	if( option4 == BG_WOOD ) this->drawCheckBox(BOX_CHECK) ;
	else this->drawCheckBox(BOX_UNCHECK) ;
	glPopName() ;
	glPopMatrix() ;
	glPushMatrix() ;
	glPushName(BG_WATER) ; // water
	glTranslatef(4,0.95,0.1) ;
	if( option4 == BG_WATER ) this->drawCheckBox(BOX_CHECK) ;
	else this->drawCheckBox(BOX_UNCHECK) ;
	glPopName() ;
	glPopMatrix() ;
	glPushMatrix() ;
	glPushName(BG_COLORFUL) ; // colorful
	glTranslatef(7.8,0.95,0.1) ;
	if( option4 == BG_COLORFUL ) this->drawCheckBox(BOX_CHECK) ;
	else this->drawCheckBox(BOX_UNCHECK) ;
	glPopName() ;
	glPopMatrix() ;
	glPopMatrix() ;
}
void ConfMenu::drawCheckBox( int check )
{
	if( BOX_CHECK == check )
	{
		glPushMatrix() ;
		this->checked->apply() ;
		glTranslated(0,0,1) ;
		glScalef(0.8,0.8,0.1) ;
		board->drawface() ;
		glPopMatrix() ;
	}
	else if( BOX_UNCHECK == check )
	{
		glPushMatrix() ;
		this->unchecked->apply() ;
		glTranslated(0,0,1) ;
		glScalef(0.8,0.8,0.1) ;
		board->drawface() ;
		glPopMatrix() ;
	}
	else
		exit(-1) ;
}

void ConfMenu::update(unsigned long t){};

int ConfMenu::pickedValue(int Value,int parentCalled)
{
	cout << "conf picked value: " << Value<< endl; 
	if(Value==BACK) //back
	{
		return BACK;
	}
	else
	{
		if(Value== DIFF_EASY || Value== DIFF_MEDIUM || Value== DIFF_HARD )
			setOption1(Value);
		else if(Value == MODE_PCvPC || Value == MODE_PvP || Value == MODE_PvPC)
			setOption2(Value);
		else if(Value == BORDER_RECT || Value == BORDER_ROUNDED)
			setOption3(Value);
		else if(Value == BG_WOOD || Value == BG_WATER || Value == BG_COLORFUL)
		{
			setOption4(Value);
			cout << "alterei" << endl;
			FLAG_APPCHANGE=true;
		}

		return CONFIG;
	}
};