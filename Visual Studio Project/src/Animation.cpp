#include "Animation.h"

void Animation::init(unsigned long t)
{
	startTime=t;
	doReset=false;
	cout << "Animation::Init-> Started Animation\n";
}

int PieceAnimation::update(unsigned long t)
{
	if (doReset)
	{
		init(t);
		MidPos.ctrlpoint[0]=(startPos.ctrlpoint[0])*5;
		MidPos.ctrlpoint[2]=(startPos.ctrlpoint[2])*5;
		MidPos.ctrlpoint[1]=(startPos.ctrlpoint[1]);

		return 0;
	}
	else 
	{
		double animT=(double)(t-startTime)/1000;
		if(animT>=duration)
		{
		//	tab->board[zi][zi]=StP;
			if(zf>=0 && zf<dimension && xf >=0 && xf<dimension)
			{
				tab->board[zf][xf]=EndP;
				done=true;
				active=false;
				return 1;
			}
			else
			{
				done=true;
				active=false;
				return 2;
			}
		}
		else
		{
			MidPos.ctrlpoint[0]=(animT/duration)*xInc+(startPos.ctrlpoint[0])*5;
			MidPos.ctrlpoint[2]=(animT/duration)*zInc+(startPos.ctrlpoint[2])*5;
			MidPos.ctrlpoint[1]=sin((animT/(duration-0.1))*PI)*MaxY+(startPos.ctrlpoint[1]);
			//cout << "updating: (" << MidPos.ctrlpoint[0] << "," << MidPos.ctrlpoint[1] << ","<<MidPos.ctrlpoint[2]<<")\n";
			return 0;
		}
	}
}

void PieceAnimation::draw()
{
	if(active)
	{
		glPushMatrix();
		glTranslated(-dimension*5/2,0,-dimension*5/2) ;
		glTranslatef(MidPos.ctrlpoint[0]+2.5,MidPos.ctrlpoint[1]+2.5,MidPos.ctrlpoint[2]+2.5);
		piece->draw();
		glPopMatrix();
	}
}