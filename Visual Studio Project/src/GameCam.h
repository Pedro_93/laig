#ifndef CAMERA_H_
#define CAMERA_H_

#include <stdlib.h>
#include <iostream>
#include <string>
#include "CGFcamera.h"
#include <GL/glut.h>

#undef near
#undef far

//#define PI 3.14159265359
#define PI_VALUE  acos(-1.0)
#define ANG_TO_RAD	PI_VALUE/180

#define BS_NORTH	1
#define BS_SOUTH	2

using namespace std;
class camera: public CGFcamera
{
protected:
	string id;
	static long LastNumber; // declaration
	const long number;
	float near ;
	float far ;
	float center1[3];
	float center2[3];
public:
	camera(string ID, float near, float far, float *cen1=NULL , float *cen2=NULL):number(++LastNumber){
		id = ID;
		this->near = near;
		this->far = far;
		if(cen1!=NULL && cen2 != NULL)
		{
			for(unsigned int i=0;i<3;i++)
			{
				center1[i]=cen1[i];
				center2[i]=cen2[i];
			}
		}
	}

	virtual ~camera(){};

	virtual bool verify()
	{
		if(id!="")
		{	return true;}
		else
		{
			perror("invalid id");
			exit(-1);
		}

		if(near<0 || far<0)
		{
			perror("invalid perspective parameters near/far values");
			exit(-1);
		}

		return true;
	}

	virtual void print()
	{
		printf("Id:%s, number: %lu\n\tNear: %f\n\tFar: %f\n\t",id.c_str(),number,near,far);
	}

	void setId(string id) {
		this->id = id;
	}

	string *getId()  {
		return &id;
	}

	long getNumber() const
	{
		return number;
	}

	virtual void applyView()=0;
	virtual void updateProjectionMatrix(int width, int height)=0;
	virtual bool camRotate(float totalAngle,float angleInc,int board_side)=0;
};

class perspective: public camera
{
private:
	float angle;
	float auxAngle;
	bool rotate;
public:
	perspective(string ID, float near, float far, float ang, float* position1 , float* position2 , float *tar):camera(ID,near,far,position1,position2){
		angle=ang;
		rotate=false;
		auxAngle=0;
		for(int i=0;i<3;i++)
		{
			this->position[i]=position1[i];
			target[i]=tar[i];
		}
	}

	virtual ~perspective(){}

	virtual bool verify(){
		camera::verify();
		if(angle<0)
		{
			perror("invalid perspective parameters angle values");
			return false;
		}
		return true;
	}

	virtual void print()
	{
		printf("Camera: Perspective, ");
		camera::print();
		printf("Type perspective\n\tAngle: %f\n\tPos: %f,%f,%f\n\tTarget: %f,%f,%f\n"
			,angle,position[0],position[1],position[2],target[0],target[1],target[2]);
	}

	//CGFCamera functions
	void applyView()
	{
		gluLookAt(position[0],position[1],position[2],target[0],target[1],target[2],0.0,1.0,0.0);
	};

	void updateProjectionMatrix(int width, int height)
	{
		float aspect= (float)width / (float)height;
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		gluPerspective(angle,aspect,near,far);
	};

	bool camRotate(float totalAngle,float angleInc, int board_side ){
		
		if( board_side != BS_NORTH && board_side != BS_SOUTH )
		{
			rotate=false;
			return true;
		}

		if(!rotate)
		{auxAngle=0;rotate=true;}

		if(auxAngle>=totalAngle)
		{
			rotate=false;
			return true;
		}
	
		auxAngle+=angleInc;
		float seno , coseno ;
		seno = sin( auxAngle*ANG_TO_RAD ) ;
		coseno = cos( auxAngle*ANG_TO_RAD ) ;

		if( board_side == BS_SOUTH )
		{
			//cout << "vou para o dois" << endl ;
			position[0]=center1[0]*coseno +   center1[2]*seno;
			position[2]=center1[0]*-seno  +   center1[2]*coseno;
		}
		else
		{
			//cout << "vou para o um" << endl ;
			position[0]=center2[0]*coseno +   center2[2]*seno;
			position[2]=center2[0]*-seno  +   center2[2]*coseno;
		}

		//cout << "auxAngle: " << auxAngle <<", pos[0]: " << position[0] ;
		//cout << ", pos[2]: " << position[2] << endl;

		return false;
	}
};

class ortho: public camera
{
private:
	float left;
	float right;
	float top;
	float bottom;
public:
	ortho(string ID, float near, float far, float left, float right, float top, float bottom):camera(ID,near,far){
		this->left=left;
		this->right=right;
		this->top=top;
		this->bottom=bottom;
	}

	virtual ~ortho(){}

	virtual bool verify(){
		camera::verify();
		return true;
	}

	virtual void print()

	{
		printf("Camera: Ortho, ");
		camera::print();
		printf("Type ortho\n\tLeft: %f\n\tRight: %f\n\tTop: %f\n\tBottom: %f\n"
			,left,right,top,bottom);
	}

	//CGFCamera functions
	void applyView()
	{
		gluLookAt(0,0,20.0,0.0,0.0,0.0,0.0,1.0,0.0);
	};

	void updateProjectionMatrix(int width, int height)
	{
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(left, right,bottom,top,near,far);
	};

	bool camRotate(float totalAngle,float angleInc,int board_side){return true;}
};

#endif /* CAMERA_H_ */
