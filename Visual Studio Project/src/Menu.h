#ifndef GAMEMENU_H
#define GAMEMENU_H
#pragma once
#include "BoardPieces.h"
#include "Scene.h"
#include "CGFlight.h"

#include <iostream>


class Menu:public Scene {
private:
	UnitCube * board ;
	CGFappearance * ng_tab ;
	CGFappearance * exit_tab ;
	CGFappearance * hs_tab ;
	CGFappearance * config_tab ;
	CGFappearance * background ;
	CGFappearance * symbol ;
	CGFlight* light0;
	camera * initialCamera ;
public:
	Menu();
	virtual ~Menu();
	void draw();
	string getName(){return "Menu";}
	void display();
	void update(unsigned long t);
	int pickedValue(int Value,int parentCalled=1);
	void keyPressed(char key){};
	void init();
	void leave();
} ;
#endif