#ifndef BOARDSPOT_H
#define BOARDSPOT_H

#include "CGFappearance.h"
#include <string>

#include <GL/gl.h>
#include <GL/glu.h>
#include <stdio.h>
#include <math.h>
#include <GL/glut.h>
#include <stdlib.h>

#include "Primitives.h"

////////////////////////////////////////////////////////////////////////
////////////					Board Spot					////////////
////////////////////////////////////////////////////////////////////////

class BoardSpot {
private:
	float BOARD_AMBIENT[4] ;
	float BOARD_SPECULAR[4];
	float BOARD_DIFFUSE[4];

	float textS , textT ;
	UnitCube * base ;
	CGFappearance * app_base ;
	CGFappearance * app_corner ;
	int x , y ;
public:
	BoardSpot( string ab , string ac , float text_s=0 , float text_t=0 )
	{
		BOARD_AMBIENT[0]=0.5 ; BOARD_AMBIENT[1]=0.5 ; BOARD_AMBIENT[2]=0.5 ; BOARD_AMBIENT[3]=0.1 ;
		BOARD_SPECULAR[0]=0 ; BOARD_SPECULAR[1]=0 ; BOARD_SPECULAR[2]=0 ; BOARD_SPECULAR[3]=0 ;
		BOARD_DIFFUSE[0]=0.8; BOARD_DIFFUSE[1]=0.8; BOARD_DIFFUSE[2]=0.8; BOARD_DIFFUSE[3]=1.0 ;

		if( text_s!=0 && text_t!=0 )
		{
			this->textT = text_t ;
			this->textS = text_s ;
		}
		else
		{
			this->textT = 1.0f ;
			this->textS = 1.0f ;
		}
		
		this->base = new UnitCube() ;
		this->app_base = new CGFappearance(ab.c_str(),textS,textT) ;
		app_base->setAmbient(BOARD_AMBIENT) ;
		app_base->setDiffuse(BOARD_DIFFUSE) ;
		app_base->setSpecular(BOARD_SPECULAR) ;
		this->app_corner = new CGFappearance(ac.c_str(),textS,textT) ;
		app_corner->setAmbient(BOARD_AMBIENT) ;
		app_corner->setDiffuse(BOARD_DIFFUSE) ;
		app_corner->setSpecular(BOARD_SPECULAR) ;
	}
	
	virtual ~BoardSpot()
	{
		delete(base);
		delete(app_base);
		delete(app_corner);
	}
	
	void draw()
	{
		// base
		glPushMatrix() ;
			glPushName( x ) ; glPushName( y ) ;
			app_base->apply() ;
			glTranslatef(0.0,0.5,0.0) ;
			glScalef(5.0,1.0,5.0) ;
			base->draw() ;
			glPopName() ; glPopName() ;
		glPopMatrix() ;

		Trapeze * t = new Trapeze() ;
		// back left
		glPushMatrix() ;
			app_corner->apply() ;
			glTranslated(-1.75,1.15,-1.75);
			glRotated(90,1,0,0) ;
			glScaled(1.5,1.5,0.3) ;
			t->draw() ;
		glPopMatrix() ;

		// front left
		glPushMatrix() ;
			app_corner->apply() ;
			glTranslated(-1.75,1.15,1.75);
			glRotated(-90,1,0,0) ;
			glScaled(1.5,1.5,0.3) ;
			t->draw() ;
		glPopMatrix() ;

		// front left
		glPushMatrix() ;
			app_corner->apply() ;
			glTranslated(1.75,1.15,-1.75);
			glRotated(-90,0,1,0) ;
			glRotated(90,1,0,0) ;
			glScaled(1.5,1.5,0.3) ;
			t->draw() ;
		glPopMatrix() ;

		// front right
		glPushMatrix() ;
			app_corner->apply() ;
			glTranslated(1.75,1.15,1.75);
			glRotated(90,0,1,0) ;
			glRotated(-90,1,0,0) ;
			glScaled(1.5,1.5,0.3) ;
			t->draw() ;
		glPopMatrix() ;
	}

	void setX( int new_x ) { this->x = new_x ; }
	void setY( int new_y ) { this->y = new_y ; }
	int getX() { return this->x ; }
	int getY() { return this->y ; }

	void setAppearance( int App )
	{
		app_base->~CGFappearance() ;
		app_corner->~CGFappearance() ;

		switch( App )
		{
		case BG_WOOD :
			app_base = new CGFappearance("../data/solid/white.jpg",1,1) ;
			app_base->setAmbient(BOARD_AMBIENT) ;
			app_base->setDiffuse(BOARD_DIFFUSE) ;
			app_base->setSpecular(BOARD_SPECULAR) ;
			app_corner = new CGFappearance("../data/solid/red.jpg",1,1) ;
			app_corner->setAmbient(BOARD_AMBIENT) ;
			app_corner->setDiffuse(BOARD_DIFFUSE) ;
			app_corner->setSpecular(BOARD_SPECULAR) ;
			break ;
		case BG_WATER :
			app_base = new CGFappearance("../data/solid/blue_light.jpg",1,1) ;
			app_base->setAmbient(BOARD_AMBIENT) ;
			app_base->setDiffuse(BOARD_DIFFUSE) ;
			app_base->setSpecular(BOARD_SPECULAR) ;
			app_corner = new CGFappearance("../data/solid/blue_navy.jpg",1,1) ;
			app_corner->setAmbient(BOARD_AMBIENT) ;
			app_corner->setDiffuse(BOARD_DIFFUSE) ;
			app_corner->setSpecular(BOARD_SPECULAR) ;
			break ;
		case BG_COLORFUL :
			app_base = new CGFappearance("../data/solid/white.jpg" , 1 , 1) ;
			app_base->setAmbient(BOARD_AMBIENT) ;
			app_base->setDiffuse(BOARD_DIFFUSE) ;
			app_base->setSpecular(BOARD_SPECULAR) ;
			app_corner = new CGFappearance("../data/solid/yellow.jpg" , 1 , 1 ) ;
			app_corner->setAmbient(BOARD_AMBIENT) ;
			app_corner->setDiffuse(BOARD_DIFFUSE) ;
			app_corner->setSpecular(BOARD_SPECULAR) ;
			break ;
		}
	}
} ;

class BoardBorder
{
private:
	float BOARD_AMBIENT[4] ;
	float BOARD_SPECULAR[4];
	float BOARD_DIFFUSE[4];

	float textS , textT ;
	UnitCube * base ;
	Trapeze * corner ;
	CGFappearance * app_base ;
	CGFappearance * table ;
	void drawRoundedCube()
	{
		//  / A | 12 | 8  | B \
		// | 9  | 2  | 4  | 6  |
		// | 10 | 3  | 1  | 5  |
		//  \ D | 11 | 7  | C /

		glPushMatrix() ; // A
			glTranslated(-0.25,0,-0.25) ;
			glRotated(180,0,1,0) ;
			glScaled(0.25,1,0.25);
			this->drawQuarterCylinder() ;
		glPopMatrix() ;
		glPushMatrix() ; // B
			glTranslated(+0.25,0,-0.25) ;
			glRotated(90,0,1,0) ;
			glScaled(0.25,1,0.25);
			this->drawQuarterCylinder() ;
		glPopMatrix() ;
		glPushMatrix() ; // C
			glTranslated(+0.25,0, +0.25) ;
			glScaled(0.25,1,0.25);
			this->drawQuarterCylinder() ;
		glPopMatrix() ;
		glPushMatrix() ; // D
			glTranslated(-0.25,0,+0.25) ;
			glRotated(-90,0,1,0) ;
			glScaled(0.25,1,0.25);
			this->drawQuarterCylinder() ;
		glPopMatrix() ;
		
		glPushMatrix() ; // 1
			glTranslated(0.25/2.0,0.5,0.25/2.0) ;
			glScaled(0.25,1,0.25);
			base->draw() ;
		glPopMatrix() ;
		glPushMatrix() ; // 2
			glTranslated(-0.25/2.0,0.5,-0.25/2.0) ;
			glScaled(0.25,1,0.25);
			base->draw() ;
		glPopMatrix() ;
		glPushMatrix() ; // 3
			glTranslated(-0.25/2.0,0.5,0.25/2.0) ;
			glScaled(0.25,1,0.25);
			base->draw() ;
		glPopMatrix() ;
		glPushMatrix() ; // 4
			glTranslated(0.25/2.0,0.5,-0.25/2.0) ;
			glScaled(0.25,1,0.25);
			base->draw() ;
		glPopMatrix() ;
		glPushMatrix() ; // 5
			glTranslated(0.25/2.0 +0.25,0.5,0.25/2.0) ;
			glScaled(0.25,1,0.25);
			base->draw() ;
		glPopMatrix() ;
		glPushMatrix() ; // 6
			glTranslated(0.25/2.0 +0.25,0.5,-0.25/2.0) ;
			glScaled(0.25,1,0.25);
			base->draw() ;
		glPopMatrix() ;
		glPushMatrix() ; // 7
			glTranslated(0.25/2.0,0.5,0.25/2.0 +0.25) ;
			glScaled(0.25,1,0.25);
			base->draw() ;
		glPopMatrix() ;
		glPushMatrix() ; // 8
			glTranslated(0.25/2.0,0.5,-0.25/2.0 -0.25) ;
			glScaled(0.25,1,0.25);
			base->draw() ;
		glPopMatrix() ;
		glPushMatrix() ; // 9
			glTranslated(-0.25/2.0 -0.25,0.5,-0.25/2.0) ;
			glScaled(0.25,1,0.25);
			base->draw() ;
		glPopMatrix() ;
		glPushMatrix() ; // 10
			glTranslated(-0.25/2.0 -0.25,0.5,0.25/2.0) ;
			glScaled(0.25,1,0.25);
			base->draw() ;
		glPopMatrix() ;
		glPushMatrix() ; // 11
			glTranslated(-0.25/2.0,0.5,0.25/2.0 +0.25) ;
			glScaled(0.25,1,0.25);
			base->draw() ;
		glPopMatrix() ;
		glPushMatrix() ; // 12
			glTranslated(-0.25/2.0,0.5,-0.25/2.0 -0.25) ;
			glScaled(0.25,1,0.25);
			base->draw() ;
		glPopMatrix() ;
	}
	void drawQuarterCylinder()
	{
		int slices = 10 ;	float textS = 1.0/slices;
		int stacks = 10 ;	float textT = 1.0/stacks;

		glPushMatrix() ;
		double angle = acos(-1.0)/(slices*2);
		for ( double j = 1 ; j <= stacks; j++ )
			for ( int i = 0; i < slices; i++ ) // body
			{
				glBegin(GL_QUADS);			
					glNormal3d(cos(angle*(i)),0,sin(angle*(i)));
					glTexCoord2d(textS*i,textT*j);
					glVertex3d(cos(angle*i) , j/stacks , sin(angle*i));
				
					glNormal3d(cos(angle*(i+1)),0,sin(angle*(i+1)));
					glTexCoord2d(textS*i,textT*j);
					glVertex3d(cos(angle*(i+1)) , j/stacks , sin(angle*(i+1)));
					glTexCoord2d(textS*i,textT*j);
					glVertex3d(cos(angle*(i+1)) , (j-1)/stacks , sin(angle*(i+1)));
				
					glNormal3d(cos(angle*(i)),0,sin(angle*(i)));
					glTexCoord2d(textS*i,textT*j);
					glVertex3d(cos(angle*i) , (j-1)/stacks , sin(angle*i));
				glEnd();
			}
		glBegin(GL_POLYGON) ; // bottom
			glNormal3d(0,-1,0) ;
			glTexCoord2d(0.5,0.5);
			glVertex3d( 0 , 0 , 0 );
			for ( int i = 0 ; i<=slices ; i++ )
			{
				glTexCoord2d( (cos(angle*i)+1)/2 , (sin(angle*i)+1)/2 );
				glVertex3d( cos(angle*i) , 0 , sin(angle*i) );
			}
			glTexCoord2d(0.5,0.5);
			glVertex3d( 0 , 0 , 0 );
		glEnd() ;
		glBegin(GL_POLYGON)	; // top
			glNormal3d(0,1,0) ;
			glTexCoord2d(0.5,0.5);
			glVertex3d( 0 , 1 , 0 );
			for ( int i = slices ; i>=0 ; i-- )
			{	
				glTexCoord2d( (cos(angle*i)+1)/2 , (sin(angle*i)+1)/2 );
				glVertex3d( cos( angle*i) , 1 , sin(angle*i) ) ;
			}
			glTexCoord2d(0.5,0.5);
			glVertex3d( 0 , 1 , 0 );
		glEnd() ;

		glPopMatrix() ;
		glPushMatrix();
			glTranslated(0.5,0.5,0) ;
			glRotated(180,0,1,0) ;
			base->drawface() ;
		glPopMatrix();
		glPushMatrix();
			glTranslated(0,0.5,0.5) ;
			glRotated(-90,0,1,0) ;
			base->drawface() ;
		glPopMatrix();
	}
public:
	BoardBorder( string ab , string tb , float text_s=0 , float text_t=0 )
	{

		BOARD_AMBIENT[0]=0.5 ; BOARD_AMBIENT[1]=0.5 ; BOARD_AMBIENT[2]=0.5 ; BOARD_AMBIENT[3]=0.1 ;
		BOARD_SPECULAR[0]=0 ; BOARD_SPECULAR[1]=0 ; BOARD_SPECULAR[2]=0 ; BOARD_SPECULAR[3]=0 ;
		BOARD_DIFFUSE[0]=0.8; BOARD_DIFFUSE[1]=0.8; BOARD_DIFFUSE[2]=0.8; BOARD_DIFFUSE[3]=1.0 ;

		if( text_s!=0 && text_t!=0 )
		{
			this->textT = text_t ;
			this->textS = text_s ;
		}
		else
		{
			this->textT = 1.0f ;
			this->textS = 1.0f ;
		}
		
		this->base = new UnitCube() ;
		this->corner = new Trapeze() ;
		this->app_base = new CGFappearance(ab.c_str(),textS,textT) ;
		app_base->setAmbient(BOARD_AMBIENT) ;
		app_base->setDiffuse(BOARD_DIFFUSE) ;
		app_base->setSpecular(BOARD_SPECULAR) ;
		this->table = new CGFappearance(tb.c_str(), textS, textT) ;
		table->setAmbient(BOARD_AMBIENT) ;
		table->setDiffuse(BOARD_DIFFUSE) ;
		table->setSpecular(BOARD_SPECULAR) ;
	}
	
	virtual ~BoardBorder()
	{
		delete(base) ;
		delete(corner) ;
		delete(app_base) ;
	}
	
	void drawHorizontal()
	{
		glPushMatrix() ;
			app_base->apply() ;
			glTranslatef(0.0,0.5,0.0) ;
			glScalef(5.0,1.0,2.0) ;
			base->draw() ;
		glPopMatrix() ;
	}
	void drawVertical()
	{
		glPushMatrix() ;
			app_base->apply() ;
			glTranslatef(0.0,0.5,0.0) ;
			glScalef(2.0,1.0,5.0) ;
			base->draw() ;
		glPopMatrix() ;
	}
	void drawCorner(int roundCorner)
	{
		glPushMatrix() ; // front left - roundCorner==3
			if( roundCorner == 3 )
			{
				glRotated(-90,0,1,0) ;
				glTranslated(0,-0.5,0);
				this->drawQuarterCylinder() ;
			}
			else
			{
				app_base->apply() ;
				glTranslated(-0.5,0,0.5) ;
				base->draw();
			}
		glPopMatrix() ;
		glPushMatrix() ; // front right - roundCorner==4
			if( roundCorner == 4 )
			{
				glTranslated(0,-0.5,0);
				this->drawQuarterCylinder() ;
			}
			else
			{
				app_base->apply() ;
				glTranslated(0.5,0,0.5) ;
				base->draw() ;
			}
		glPopMatrix() ;
		glPushMatrix() ; // back left - roundCorner==1
			if( roundCorner == 1 )
			{
				glRotated(180,0,1,0) ;
				glTranslated(0,-0.5,0);
				this->drawQuarterCylinder() ;
			}
			else
			{
				app_base->apply() ;
				glTranslated(-0.5,0,-0.5) ;
				base->draw() ;
			}
		glPopMatrix() ;
		glPushMatrix() ; // back right - roundCorner==2
			if( roundCorner == 2 )
			{
				glRotated(90,0,1,0) ;
				glTranslated(0,-0.5,0);
				this->drawQuarterCylinder() ;
			}
			else
			{
				app_base->apply() ;
				glTranslated(0.5,0,-0.5) ;
				base->draw() ;
			}
		glPopMatrix() ;
	}
	void drawTriangleCorner()
	{
		glPushMatrix() ;
			glRotated(-90,1,0,0) ;
			corner->draw() ;
		glPopMatrix() ;
	}
	
	void drawTable( int size , int round )
	{
		glPushMatrix() ;
			if( round == 0 )
			{
				this->table->apply() ;
				glTranslated(5*size/2,-2,5*size/2) ;
				glScaled(5*size+10,1,5*size+25) ;
				base->draw() ;
			}
			else
			{
				glTranslated(5*size/2,-2,5*size/2) ;
				glScaled(5*size+10,1,5*size+25) ;
				this->table->apply() ;
				this->drawRoundedCube() ;
			}
		glPopMatrix() ;
	}

	void setAppearance( int App )
	{
		app_base->~CGFappearance() ;
		table->~CGFappearance() ;

		switch( App )
		{
		case BG_WOOD :
			app_base = new CGFappearance("../data/solid/red.jpg",1,1) ;
			app_base->setAmbient(BOARD_AMBIENT) ;
			app_base->setDiffuse(BOARD_DIFFUSE) ;
			app_base->setSpecular(BOARD_SPECULAR) ;
			table = new CGFappearance("../data/solid/brown.jpg",1,1) ;
			table->setAmbient(BOARD_AMBIENT) ;
			table->setDiffuse(BOARD_DIFFUSE) ;
			table->setSpecular(BOARD_SPECULAR) ;
			break ;
		case BG_WATER :
			app_base = new CGFappearance("../data/solid/blue_navy.jpg",1,1) ;
			app_base->setAmbient(BOARD_AMBIENT) ;
			app_base->setDiffuse(BOARD_DIFFUSE) ;
			app_base->setSpecular(BOARD_SPECULAR) ;
			table = new CGFappearance("../data/solid/white.jpg",1,1) ;
			table->setAmbient(BOARD_AMBIENT) ;
			table->setDiffuse(BOARD_DIFFUSE) ;
			table->setSpecular(BOARD_SPECULAR) ;
			break ;
		case BG_COLORFUL :
			app_base = new CGFappearance("../data/solid/green_dark.jpg" , 1 , 1) ;
			app_base->setAmbient(BOARD_AMBIENT) ;
			app_base->setDiffuse(BOARD_DIFFUSE) ;
			app_base->setSpecular(BOARD_SPECULAR) ;
			table = new CGFappearance("../data/solid/green_light.jpg" , 1 , 1 ) ;
			table->setAmbient(BOARD_AMBIENT) ;
			table->setDiffuse(BOARD_DIFFUSE) ;
			table->setSpecular(BOARD_SPECULAR) ;
			break ;
		}
	}
} ;
#endif