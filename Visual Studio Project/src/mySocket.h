#ifndef MYSOCKET_H
#define MYSOCKET_H

#pragma once

#include <winsock2.h>
#include <string>
#include <iostream>
#include <sstream>

#define IPADDRESS "127.0.0.1"
#define PORT 60070

class mySocket
{
private:
	SOCKET m_socket;
public:
	mySocket() {};
	
	~mySocket(void);

	bool startConnection();

	void envia(char *s, int len);

	void recebe(char *ans);

	void getTabuleiro();

	char * criaTabuleiro(int size);

	char * setPeca(std::string tab,int X,int Y, int Value);
	
	int validateMove(std::string tab,int size, int Xi, int Yi, int Xf, int Yf,int Value, int activePlayer);

	int validateBonusMove(std::string tab,int size, int Xi, int Yi, int Xf, int Yf,int Value, int bonusType, int activePlayer) ;

	int validateEndGame(std::string tab, int Xi, int Yi, int Xf, int Yf,int Value,int Player);

	void quit();
};

#endif