#ifndef PRIMITIVES_H
#define PRIMITIVES_H

#include "CGFappearance.h"
#include "CGFobject.h"

#include <string>

#include <GL/gl.h>
#include <GL/glu.h>
#include <stdio.h>
#include <math.h>
#include <GL/glut.h> // nao tens o glu.h ? tenho mas nunca precisei nos projectos, o que eu vi e que tens uma tonelada de includes repetidos. Prob
#include <stdlib.h>

////////////////////////////////////////////////////////////////////////
////////////					Primitives					////////////
////////////////////////////////////////////////////////////////////////


class point
{
public:
	GLfloat ctrlpoint[3] ;

	point(GLfloat x1, GLfloat y1, GLfloat z1)
	{
		ctrlpoint[0] = x1 ;
		ctrlpoint[1] = y1 ;
		ctrlpoint[2] = z1 ;
	}
	point()
	{
		ctrlpoint[0] = 0.0 ;
		ctrlpoint[1] = 0.0 ;
		ctrlpoint[2] = 0.0 ;
	}

	void print()
	{
		printf("Control Point :: (x,y,z)=(%f,%f,%f)\n", ctrlpoint[0] , ctrlpoint[1], ctrlpoint[2] ) ;
	}
};

class Cylinder
{
private:
	float base , top , height , textS , textT ;
	int slices,stacks;
	GLUquadric* quad;
public:
	Cylinder(float base, float top, float height, int slices, int stacks,float text_S ,float text_T)
	{
		this->base=base ;
		this->top=top ;
		this->height=height ;
		this->slices=slices ;
		this->stacks=stacks ;
		this->textS = text_S ;
		this->textT = text_T ;
		quad=gluNewQuadric();
	}

	virtual ~Cylinder(){}

	void draw()
	{
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, 1.0);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, 1.0);

		//top
		glPushMatrix() ;
		gluQuadricTexture(quad,1);
		glTranslated(0,0,height) ;
		gluDisk(quad,0,top,slices,1);
		glPopMatrix() ;

		gluCylinder(quad,base,top,height,slices,stacks);

		//bot
		glPushMatrix();
		glRotated(180,1,0,0);
		gluDisk(quad,0,base,slices,1);
		glPopMatrix();
	}
};

class Plane
{
private:
	int parts ;
	point ctrlpoints[4] ;
	GLfloat textpoints[4][2] ;
	float textT , textS ;
public:
	Plane(int parts, float text_S , float text_T)
	{
		this->parts = parts ;

		// CONTROL POINTS
		// P3 ---- P4
		// |       |
		// P1 ---- P2
		/* p1 */ ctrlpoints[0] = point(-0.5 , 0.0 , 0.5 ) ;
		/* p2 */ ctrlpoints[1] = point( 0.5 , 0.0 , 0.5 ) ;
		/* p3 */ ctrlpoints[2] = point(-0.5 , 0.0 , -0.5) ;
		/* p4 */ ctrlpoints[3] = point( 0.5 , 0.0 , -0.5 ) ;

		// TEXT POINTS
		/* t1 */ textpoints[0][0] = 0.0 ; textpoints[0][1] = 0.0 ;
		/* t2 */ textpoints[1][0] = 1.0 ; textpoints[1][1] = 0.0 ;
		/* t3 */ textpoints[2][0] = 0.0 ; textpoints[2][1] = 1.0 ;
		/* t4 */ textpoints[3][0] = 1.0 ; textpoints[3][1] = 1.0 ;

		this->textS = text_S ;
		this->textT = text_T ;
	}

	void draw()
	{
		glColor3f(1.0,1.0,1.0) ;
		glFrontFace(GL_CW) ;

		glMap2f(GL_MAP2_VERTEX_3, 0.0, 1.0, 3, 2,  0.0, 1.0, 6, 2,  &ctrlpoints[0].ctrlpoint[0]) ;
		glMap2f(GL_MAP2_TEXTURE_COORD_2,  0.0, 1.0, 2, 2,  0.0, 1.0, 4, 2,  &textpoints[0][0]) ;

		glEnable(GL_MAP2_VERTEX_3) ;
		glEnable(GL_AUTO_NORMAL) ;
		glEnable(GL_MAP2_TEXTURE_COORD_2) ;

		glMapGrid2f(parts, 0.0,1.0, parts, 0.0,1.0) ; 

		glEnable(GL_LIGHTING);
		glEnable(GL_COLOR_MATERIAL);
		glEnable(GL_TEXTURE_2D);

		glEvalMesh2(GL_FILL, 0,parts, 0,parts) ;

		glFrontFace(GL_CCW);
	}

	void calculateTexturePoints(){
		// TEXT POINTS
		/* t1 */ textpoints[0][0] = 0.0 ; textpoints[0][1] = 0.0 ;
		/* t2 */ textpoints[1][0] = 0.0 ; textpoints[1][1] = textT ;
		/* t3 */ textpoints[2][0] = textS ; textpoints[2][1] = 0.0 ;
		/* t4 */ textpoints[3][0] = textT ; textpoints[3][1] = textT ;
	}
};


class UnitCube: public CGFobject {
	float s ;
	float t ;
public:
	UnitCube(float s=1 ,float t=1):s(s),t(t)
	{}
	void draw()
	{
		/* ============ square foundation for the construction of the unit cube ============ */
		// ---> glRectd(-0.5,-0.5,0.5,0.5) ; with this rectangle, we will build a 3d cube

		/****************************************/ // FRONT SURFACE :: 0.5*z
		glPushMatrix() ;						//
		glTranslated(0,0,0.5) ;				// This creates the first surface in z plane :: + 0.5 z
		glNormal3f(0,0,1);					//
		drawface();
		//glRectd(-0.5,-0.5,0.5,0.5) ;		//
		glPopMatrix() ;							//
		/***************************************/

		/****************************************/ // BACK SURFACE :: -0.5*z
		glPushMatrix() ;						//
		glTranslated(0,0,-0.5) ;			// This creates the oposite surface in z plane :: - 0.5 z
		glRotated(180,0,1,0);				//
		glNormal3f(0,0,1);					//
		drawface();
		//glRectd(-0.5,-0.5,0.5,0.5) ;		//
		glPopMatrix() ;							//
		/***************************************/

		/****************************************/ // RIGHT SURFACE :: 0.5*x
		glPushMatrix() ;						//
		glTranslated(0.5,0,0) ;				// This creates the right surface in x plane :: 0.5 x
		glRotated(90,0,1,0);				//
		glNormal3f(0,0,1);					//
		drawface();
		//glRectd(-0.5,-0.5,0.5,0.5) ;		//
		glPopMatrix() ;							//
		/***************************************/

		/****************************************/ // LEFT SURFACE :: -0.5*x
		glPushMatrix() ;						//
		glTranslated(-0.5,0,0) ;			// This creates the left surface in x plane :: - 0.5 x
		glRotated(-90,0,1,0);				//
		glNormal3f(0,0,1);					//
		drawface();
		//glRectd(-0.5,-0.5,0.5,0.5) ;		//
		glPopMatrix() ;							//
		/***************************************/

		/****************************************/ // TOP SURFACE :: 0.5*y
		glPushMatrix() ;						//
		glTranslated(0,0.5,0) ;				// This creates the top surface in y plane :: - 0.5 x
		glRotated(-90,1,0,0);				//
		glNormal3f(0,0,1);					//
		drawface();
		//glRectd(-0.5,-0.5,0.5,0.5) ;		//
		glPopMatrix() ;							//
		/***************************************/

		/****************************************/ // BOTTOM SURFACE :: -0.5*y
		glPushMatrix() ;						//
		glTranslated(0,-0.5,0) ;			// This creates the botoom surface in y plane :: - 0.5 x
		glRotated(90,1,0,0);				//
		glNormal3f(0,0,1);					//
		drawface();
		//glRectd(-0.5,-0.5,0.5,0.5) ;		//
		glPopMatrix() ;							//
		/***************************************/
	}
	void drawface() 
	{
		glBegin(GL_POLYGON) ;
		glTexCoord2d(0,0);
		glVertex3d(-0.5,-0.5,0) ;
		glTexCoord2d(s,0);
		glVertex3d(0.5,-0.5,0) ;
		glTexCoord2d(s,t);
		glVertex3d(0.5,0.5,0) ;
		glTexCoord2d(0,t);
		glVertex3d(-0.5,0.5,0) ;
		glEnd();
	}
};


class Trapeze : public CGFobject
{
private:
	CGFappearance * app ;
public:
	Trapeze(string texture){ app = new CGFappearance(texture.c_str(),1.0,1.0) ; }
	Trapeze(){}

	void draw()
	{
		// front
		glPushMatrix() ;
		glTranslated(0,0,0.5) ;
		this->drawTri() ;
		glPopMatrix() ;

		// back
		glPushMatrix() ;
		glTranslated(0,0,-0.5) ;
		glRotated(180,0,1,0);
		glRotated(90,0,0,1) ;
		this->drawTri() ;
		glPopMatrix() ;

		// left
		glPushMatrix() ;
		glTranslated(-0.5,0,0) ;
		glRotated(-90,0,1,0) ;
		this->drawRect() ;
		glPopMatrix() ;

		// rigth
		glPushMatrix() ;
		glScaled(sqrt(2.0),sqrt(2.0),1.0) ;
		glRotated(45,0,0,1) ;
		glRotated(90,0,1,0) ;
		this->drawRect() ;
		glPopMatrix() ;

		//bottom
		glPushMatrix() ;
		glTranslated(0,-0.5,0) ;
		glRotated(90,1,0,0) ;
		this->drawRect() ;
		glPopMatrix() ;
	}
	void drawRect()
	{
		glBegin(GL_POLYGON) ;
		glTexCoord2d(0,0);
		glVertex3d(-0.5,-0.5,0) ;
		glTexCoord2d(1,0);
		glVertex3d(0.5,-0.5,0) ;
		glTexCoord2d(1,1);
		glVertex3d(0.5,0.5,0) ;
		glTexCoord2d(0,1);
		glVertex3d(-0.5,0.5,0) ;
		glEnd();
	}
	void drawTri()
	{
		glBegin(GL_POLYGON) ;
		glTexCoord2d(0,0);
		glVertex3d(-0.5,-0.5,0) ;
		glTexCoord2d(1,0);
		glVertex3d(0.5,-0.5,0) ;
		glTexCoord2d(1,1);
		glVertex3d(0.0,0.0,0.0) ;
		glTexCoord2d(0,1);
		glVertex3d(-0.5,0.5,0) ;
		glEnd();
	}

} ;

#endif