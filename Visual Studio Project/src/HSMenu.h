#ifndef HSMENU_H
#define HSMENU_H

#include "BoardPieces.h"
#include "Scene.h"
#include "CGFlight.h"
#include <stdio.h>
#include <stdlib.h>

class HSMenu: public Scene {
private:
	UnitCube * board ;
	CGFappearance * back_tab ;
	CGFappearance * hs_tab ;
	CGFappearance * background ;
	CGFappearance * symbol ;
	CGFappearance * charApp ;
	camera * initialCamera ;
public:
	HSMenu();
	~HSMenu();
	void display();
	void drawHS();

	void update(unsigned long t);
	int pickedValue(int Value,int parentCalled=1);
	void keyPressed(char key);
	void init();
	void leave();
	string getName(){return "HSMenu";}
} ;

#endif