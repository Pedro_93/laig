#ifndef PickInterface_H
#define PickInterface_H

#include "CGFinterface.h"
#include <iostream>
#include "CGFapplication.h"
#include "Gyges.h"

class PickInterface: public CGFinterface {
	public:
		void initGUI();
		void processGUI(GLUI_Control* ctrl);
		virtual void processMouse(int button, int state, int x, int y);	
		void performPicking(int x, int y);
		void processHits(GLint hits, GLuint buffer[]);
		virtual void processKeyboard(unsigned char key, int x, int y);
};


#endif
