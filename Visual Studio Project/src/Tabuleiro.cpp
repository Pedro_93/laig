#include "Tabuleiro.h"

Tabuleiro::Tabuleiro(int tam)
{
	sock=new mySocket();
	this->size=tam;
	board = new int*[size];
	for(int i = 0; i < size; ++i)
		board[i] = new int[tam]();
}

bool Tabuleiro::start()
{
	activePlayer=1;
	suspendedPiece=0;
	for(int i = 0; i < size; ++i)
		for(int j = 0; j < size; ++j)
			board[i][j] = 0;
	if(!sock->startConnection())
		return false;
	fromStr(sock->criaTabuleiro(size));
	setUp(1);
	return true;
	//validateMove(1,1,1,4);
}

void Tabuleiro::reset()
{
	for(int i = 0; i < size; ++i)
		for(int j = 0; j < size; ++j)
			board[i][j] = 0;
	setUp(1);
}

void Tabuleiro::setUp(int option)
{
	switch (option)
	{
	case 1:
		setPiece(1,1,3);
		setPiece(1,2,2);
		setPiece(1,3,1);
		setPiece(1,4,1);
		setPiece(1,5,2);
		setPiece(1,6,3);

		setPiece(6,1,3);
		setPiece(6,2,2);
		setPiece(6,3,1);
		setPiece(6,4,1);
		setPiece(6,5,2);
		setPiece(6,6,3);
		break;
	case 2:
		setPiece(1,1,3);
		setPiece(1,2,2);
		setPiece(1,3,1);
		setPiece(1,4,3);
		setPiece(1,5,2);
		setPiece(1,6,1);

		setPiece(6,1,3);
		setPiece(6,2,2);
		setPiece(6,3,1);
		setPiece(6,4,3);
		setPiece(6,5,2);
		setPiece(6,6,1);
		break;
	default:
		setPiece(1,1,1);
		setPiece(1,2,1);
		setPiece(1,3,2);
		setPiece(1,4,2);
		setPiece(1,5,3);
		setPiece(1,6,3);

		setPiece(6,1,1);
		setPiece(6,2,1);
		setPiece(6,3,2);
		setPiece(6,4,2);
		setPiece(6,5,3);
		setPiece(6,6,3);
		break;
	}
}

void Tabuleiro::quit()
{
	sock->quit();
}

string Tabuleiro::toStr()
{
	stringstream aux;
	aux << "[";
	for(int i=0; i<size;i++)
	{
		if(i==size-1)
		{
			aux << "[";
			for(int j=0; j<size;j++)
			{
				if(j==size-1)
					aux <<board[i][j];
				else
					aux <<board[i][j] <<",";
			}
			aux << "]";
		}
		else {
			aux << "[";
			for(int j=0; j<size;j++)
			{
				if(j==size-1)
					aux <<board[i][j];
				else
					aux <<board[i][j] <<",";
			}
			aux << "]";
			aux <<",";
		}
	}
	aux << "]";
	return aux.str();
}

void Tabuleiro::fromStr(char* strTab)
{
	int index=0;
	strTab++; //[
	while(*strTab!='.')
	{
		strTab++;
		for(unsigned int i=0; i<size;i++)
		{
			board[index][i]=(int)(*strTab)-48;
			strTab++;
			strTab++;
		}
		/*printf("index: %d -> [",index);
		for(unsigned int i=0; i<size;i++)
		{if(i==size-1) printf("%d",tab[index][i]);
		else printf("%d,",tab[index][i]);}
		printf("]\n");*/
		index++;
		strTab++;
	}
}

void Tabuleiro::setPiece(int X, int Y, int Value)
{
	board[X-1][Y-1]=Value;
}

int Tabuleiro::getPiece(int X, int Y)
{
	return board[X][Y];
}

bool Tabuleiro::validateMove(int Xi, int Yi, int Xf, int Yf)
{
	int result = sock->validateMove(toStr(),size,Xi+1,Yi+1,Xf+1,Yf+1,board[Yi][Xi],activePlayer);
	//printf("validateMove returned %d\n",result);
	if(result > -1 && result <=3)
	{
		//printf("true\n");
		return true;
	}
	else
	{
		//printf("false\n");
		return false;
	}
}

bool Tabuleiro::bonusMove(int Xi,int Yi,int Xf,int Yf,int bonus,int bonusType)
{
	int	result = sock->validateBonusMove(toStr(),size,Xi+1,Yi+1,Xf+1,Yf+1,bonus,bonusType,activePlayer);

	//printf("validateMove returned %d\n",result);
	if(result > -1 && result <=3)
	{
		//printf("true\n");
		return true;
	}
	else
	{
		//printf("false\n");
		return false;
	}
}

void Tabuleiro::computerPlay()
{

}

bool Tabuleiro::fimJogo(int Xi, int Yi, int Xf, int Yf,int piece)
{
	int	result = sock->validateEndGame(toStr(),Xi+1,Yi+1,Xf+1,Yf+1,piece,activePlayer);

	//printf("validateMove returned %d\n",result);
	if(result ==1)
	{
		//printf("true\n");
		return true;
	}
	else
	{
		//printf("false\n");
		return false;
	}
}

void Tabuleiro::print()
{
	for(unsigned int i=0; i<size;i++)
	{
		printf("[");
		for(unsigned int j=0; j<size; j++)
		{
			printf("%d",board[i][j]);
			if(j!=size-1)
				printf(",");
		}
		printf("]");
		printf("\n");
	}
}

Tabuleiro::~Tabuleiro(void)
{
	for(int i = 0; i < size; ++i) {
		delete [] board[i];
	}
	delete [] board;
}
