#include <iostream>
#include <exception>

#include "CGFapplication.h"
#include "PickInterface.h"
#include "Gyges.h"
#include "Shared.h"

using std::cout;
using std::exception;

int main(int argc, char* argv[]) {

	CGFapplication app = CGFapplication();
	app.width=800;
	app.height=1000;
	try {
		app.init(&argc, argv);

		app.setScene(new Gyges());
		app.setInterface(new PickInterface());
		app.run();
	}
	catch(GLexception& ex) {
		cout << "Error: " << ex.what();
		return -1;
	}
	catch(exception& ex) {
		cout << "Unexpected error: " << ex.what();
		return -1;
	}

	return 0;
}