#include "Game.h"
#include <glut.h>
#include "GL/gl.h"
#include <stdio.h>
#include <iostream>
#include <windows.h>
#include <tchar.h>
using namespace std ;

int FLAG_ONLY_JUMP = 0 ;
bool FLAG_NewHS;
bool MOVIE_FLAG;
int BoardSize = 6;
int BoardPiece::counter = 0  ;
static int Goal1=0;
static int Goal2=0;
string playerName ;
static PROCESS_INFORMATION pi;

#define PLAYER_MAX_SIZE 10 
#define frameRate 30
#define REPLAY 31
#define NEWGAME 32

double gameTime=0;

Game::Game(int n , string spot1 , string spot2 , string border1 , string border2):dimension(n)
{
	if( !(n >= 6 && n <= 18 && n%3==0) )
	{
		printf("BOARD ERROR - Constructor :: Invalid size (%d)\n", n) ;
		exit(-1) ;
	}

	BOARD_AMBIENT[0]=0.5 ; BOARD_AMBIENT[1]=0.5 ; BOARD_AMBIENT[2]=0.5 ; BOARD_AMBIENT[3]=0.1 ;
	BOARD_SPECULAR[0]=0 ; BOARD_SPECULAR[1]=0 ; BOARD_SPECULAR[2]=0 ; BOARD_SPECULAR[3]=0 ;
	BOARD_DIFFUSE[0]=0.8; BOARD_DIFFUSE[1]=0.8; BOARD_DIFFUSE[2]=0.8; BOARD_DIFFUSE[3]=1.0 ;


	tab = new Tabuleiro(dimension);

	bs = new BoardSpot(spot1,spot2,5,5) ;		// chao - cantos
	bb = new BoardBorder(border1,border2) ;		// bordas - base "global"
	this->tag = new UnitCube() ;

	this->player1_tag = new CGFappearance("../data/game_board/player1.jpg",1.0,1.0) ;
	player1_tag->setAmbient(BOARD_AMBIENT) ;
	player1_tag->setDiffuse(BOARD_DIFFUSE) ;
	player1_tag->setSpecular(BOARD_SPECULAR) ;
	this->player2_tag = new CGFappearance("../data/game_board/player2.jpg",1.0,1.0) ;
	player2_tag->setAmbient(BOARD_AMBIENT) ;
	player2_tag->setDiffuse(BOARD_DIFFUSE) ;
	player2_tag->setSpecular(BOARD_SPECULAR) ;
	this->newHs_tag = new CGFappearance("../data/game_menu/name_enter.jpg",1.0,1.0) ;
	newHs_tag->setAmbient(BOARD_AMBIENT) ;
	newHs_tag->setDiffuse(BOARD_DIFFUSE) ;
	newHs_tag->setSpecular(BOARD_SPECULAR) ;
	this->charApp = new CGFappearance("../data/solid/white.jpg",1.0,1.0) ;
	charApp->setAmbient(BOARD_AMBIENT) ;
	charApp->setDiffuse(BOARD_DIFFUSE) ;
	charApp->setSpecular(BOARD_SPECULAR) ;
	this->enter_tag = new CGFappearance("../data/game_menu/enter.jpg",1.0,1.0) ;
	enter_tag->setAmbient(BOARD_AMBIENT) ;
	enter_tag->setDiffuse(BOARD_DIFFUSE) ;
	enter_tag->setSpecular(BOARD_SPECULAR) ;
	this->replay = new CGFappearance("../data/game_menu/replay.jpg",1.0,1.0) ;
	replay->setAmbient(BOARD_AMBIENT) ;
	replay->setDiffuse(BOARD_DIFFUSE) ;
	replay->setSpecular(BOARD_SPECULAR) ;

	float position1[]={0,	45,		(dimension*5/2+50)*-1+20} ;
	float position2[]={0,	45,		(dimension*5/2+50)-20} ;
	float target1[] = {0,	2,		0} ;

	//this->bonusInUse = BONUS_JUMP ;

	gameCamera = new perspective("player1",0,100,50,position1,position2,target1);

	p1 = new SinglePiece("../data/wood/wood_reflex.jpg","../data/wood/wood_reflex_red.jpg");
	p2 = new DoublePiece("../data/wood/wood_reflex.jpg","../data/wood/wood_reflex_red.jpg");
	p3 = new TriplePiece("../data/wood/wood_reflex.jpg","../data/wood/wood_reflex_red.jpg");
	anime = new PieceAnimation(tab,1.5,dimension,4);
}

Game::~Game()
{

}

void Game::init()
{
	playerName = "" ;
	startServer();
	pieceSelected=false;
	SelectedFirstCoor=false;
	GameEnded=false;
	camRotation=false;
	if(!tab->start())
	{
		return;
	}
	// Enables lighting computations
	glEnable(GL_LIGHTING);

	// Sets up some lighting parameters
	glLightModelf(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, CGFlight::background_ambient);  // Define ambient light

	// Declares and enables a light
	float light0_pos[4] = {0, 30, 0, 1.0};
	light0 = new CGFlight(GL_LIGHT0, light0_pos);
	light0->enable();

	// Defines a default normal
	glNormal3f(0,0,1);

	setUpdatePeriod(frameRate);

	this->bonusInUse = BONUS_JUMP ;
	Goal1=0;
	Goal2=0;
	FLAG_NewHS=false;
	MOVIE_FLAG=false;
	FLAG_ONLY_JUMP = 0 ;
	BoardSize = 6;
	playerName ;
	gameTime=0;
	//display();
}

void Game::leave()
{
	delete(light0);
	tab->quit();
	closeServer();
}

void Game::update(unsigned long t)
{
	if(anime->isActive())
	{
		int ret =anime->update(t);
		if(ret && MOVIE_FLAG)
		{
			if(nextMovieMove())
				return;
		}
		else if(ret && tab->suspendedPiece==0 && !GameEnded) //checks if animation has ended
			changeView();
		else if(ret==2 && GameEnded)
		{
			if(Plays->back()->getPlayer()==1)
				Goal2=Plays->back()->getEndPiece();
			else if(Plays->back()->getPlayer()==2)
				Goal1=Plays->back()->getEndPiece();
		}
	}
	if(camRotation)
	{
		//cout << "rotating camera : player " << tab->activePlayer << endl ;
		if(gameCamera->camRotate(180,10,tab->activePlayer))
			camRotation=false;
		gameCamera->applyView();
		//CGFapplication::activeApp->forceRefresh();
	}
	if(!GameEnded)
		gameTime+=frameRate;

	if( Scene::FLAG_APPCHANGE )
	{
		cout << "mudei" << endl ;
		this->setAppearance( Background ) ;
	}
}

int Game::pickedValue(int Value,int parentCalled)
{
	if(!GameEnded)
	{
		if(anime->isDone() && !camRotation)
		{
			if(!pieceSelected)
			{
				if(!SelectedFirstCoor && Value!=-1 && Value!=dimension)
				{
					selectedXi=Value;
					SelectedFirstCoor=true;
				}
				else if(SelectedFirstCoor && Value!=-1 && Value!=dimension)
				{
					selectedYi=Value;
					SelectedFirstCoor=false;
					pieceSelected=true;
				}
			}
			else
			{
				if(!SelectedFirstCoor)
				{
					selectedXf=Value;
					SelectedFirstCoor=true;
				}
				else
				{
					selectedYf=Value;
					SelectedFirstCoor=false;
					pieceSelected=false;
					movePiece(selectedXi,selectedYi,selectedXf,selectedYf);
				}
			}
			return 1;
		}
	}

	if( GameEnded &&!MOVIE_FLAG)
	{
		if(Value==BACK)
		{
			printf("\n\n HERE \n\n");
			addHighScore( hs , playerName , gameTime/1000 ) ;
			writeHighScores( HSFilename , hs ) ;

			return BACK;
		}
		else if(Value==REPLAY)
		{
			MOVIE_FLAG=true;
			startMovieAnimation();
		}
	}
	return 1;
}

void Game::keyPressed(char key)
{
	if(anime->isDone() && !MOVIE_FLAG)
	{
		if(key=='z')
		{
			if(!Plays->empty())
			{
				Move* x=Plays->back();
				if(tab->activePlayer!=x->getPlayer()) //only remove previous
				{
					Plays->pop_back();
					if(GameType!=MODE_PvP)
					{
						tab->board[x->getStart().second][x->getStart().first]=tab->board[x->getFinish().second][x->getFinish().first];
						if(tab->activePlayer==x->getPlayer())
						{
							if(tab->suspendedPiece)
							{
								tab->board[x->getFinish().second][x->getFinish().first]=tab->suspendedPiece;
								tab->suspendedPiece=0;
								pieceSelected=true;
							}
						}
						else
						{
							tab->board[x->getFinish().second][x->getFinish().first]=0;
						}
						//tab->activePlayer=x->getPlayer();
						delete(x);
						//Plays->pop_back();
					}
					else 
					{
						if(Plays->size()>0)
						{
							while(Plays->back()->getPlayer()==x->getPlayer())
							{
								if(x->getBonusPiece()!=0)
									if(tab->board[x->getFinish().second][x->getFinish().first]==x->getBonusPiece())
										tab->board[x->getStart().second][x->getStart().first]=x->getEndPiece();
									else if(x->getEndPiece()!=0)
										tab->board[x->getStart().second][x->getStart().first]=tab->board[x->getFinish().second][x->getFinish().first];

									else if(x->getEndPiece()!=0)
										tab->board[x->getStart().second][x->getStart().first]=x->getEndPiece();							
									tab->board[x->getFinish().second][x->getFinish().first]=x->getBonusPiece();
									x=Plays->back();
									Plays->pop_back();
							}
							tab->board[x->getStart().second][x->getStart().first]=x->getEndPiece();
							tab->board[x->getFinish().second][x->getFinish().first]=x->getBonusPiece();
							//Plays->pop_back();
						}
						else
						{
							tab->board[x->getStart().second][x->getStart().first]=tab->board[x->getFinish().second][x->getFinish().first];
							tab->board[x->getFinish().second][x->getFinish().first]=x->getBonusPiece();
						}
						delete(x);
					}
				}
			}
		}
		if(key=='s' && tab->suspendedPiece!=0 && FLAG_ONLY_JUMP==0)
		{
			//cout << "TROCA DE BONUS = " ;
			if(	this->bonusInUse == BONUS_JUMP )
			{
				//cout << "muda para troca" << endl ;
				this->bonusInUse = BONUS_SWITCH ;
			}
			else
			{
				//cout << "muda para salta" << endl ;
				this->bonusInUse = BONUS_JUMP ;
			}
		}
	}

	if( GameEnded && FLAG_NewHS && !MOVIE_FLAG) 
	{
		int ASCIIcode = key ;
		if( ASCIIcode==8 ) // apagar
		{
			playerName = playerName.substr(0,playerName.size()-1) ;
			cout << playerName << endl ;
		}

		if( playerName.size() == PLAYER_MAX_SIZE )
			return ;

		// grandes
		if( ASCIIcode>=65 && ASCIIcode<=90 )
		{
			playerName+=key ;
			cout << playerName << endl ;
		}
		else
			if( ASCIIcode>=97 && ASCIIcode<=121 )
			{
				playerName+=key ;
				cout << playerName << endl ;
			}
	}
}

void Game::display()
{
	gameCamera->applyView();

	if( FLAG_NewHS && !anime->isActive() && GameEnded && !MOVIE_FLAG)
	{
		drawTextBox() ;
	}
	else if ( !anime->isActive() && GameEnded && !MOVIE_FLAG)
	{
		drawBackButton() ;
	}
	switch(BoardDraw)
	{
	case BORDER_ROUNDED :
		this->drawWithRoundedCorners() ;
		break ;
	case BORDER_RECT :
		this->drawWithRectCorners() ;
		break;
	default:
		break ;
	}
	drawPieces();
	if(anime->isActive())
		anime->draw();
	drawTime();
}
void Game::drawWithRoundedCorners()
{
	glPushMatrix() ;
	glTranslated(-dimension*5/2,0,-dimension*5/2) ;
	this->drawBody() ;
	this->drawGoals() ;
	this->drawTable(dimension,1) ;
	this->drawGoalBorder(1) ;
	this->drawBoardBorders(1) ;
	this->drawPlayerTag();
	glPopMatrix() ;
}
void Game::drawWithRectCorners()
{
	glPushMatrix() ;
	glTranslated(-dimension*5/2,0,-dimension*5/2) ;
	this->drawBody() ;
	this->drawGoals() ;
	this->drawTable(dimension,0) ;
	this->drawGoalBorder(0) ;
	this->drawBoardBorders(0) ;
	this->drawPlayerTag();
	glPopMatrix() ;
}
void Game::drawGoals()
{
	// goal 1 -------------------------------------------------
	glPushMatrix() ;
	glTranslated(2.5,0,2.5) ;
	glTranslated((5*dimension/2)-2.5,0,-7) ;
	bs->setX(dimension/2 +0.5) ;
	bs->setY(-1) ;
	bs->draw() ;
	glPushMatrix();
	glTranslatef(0,1,0);
	drawGoalPiece(Goal1);
	glPopMatrix();
	glPopMatrix() ;

	// goal 2 -------------------------------------------------
	glPushMatrix() ;
	glTranslated(2.5,0,2.5) ;
	glTranslated((5*dimension/2)-2.5,0,5*dimension + 2) ;
	bs->setX(dimension/2+0.5) ;
	bs->setY(dimension) ;
	bs->draw() ;
	glPushMatrix();
	glTranslatef(0,1,0);
	drawGoalPiece(Goal2);
	glPopMatrix();

	glPopMatrix() ;
}
void Game::drawTable(int n, int b)
{
	// table where the board is set -----
	glPushMatrix() ;
	glTranslated(0,1,0);
	bb->drawTable(n,b) ;
	glPopMatrix() ;
}
void Game::drawBody()
{
	// board body ------------------------------
	glPushMatrix() ;
	int i=0 , k=0;
	for( i=0 ; i<dimension ; i++ )
		for( k=0 ; k<dimension ; k++ )
		{
			glPushMatrix() ;
			glTranslated(2.5,0,2.5) ;
			glTranslated(i*5,0,k*5) ;
			bs->setX(i) ;
			bs->setY(k) ;
			bs->draw() ;
			glPopMatrix() ;
		}
		glPopMatrix() ;
}
void Game::drawGoalBorder(int rounded)
{
	// back border -------------------------------------------
	glPushMatrix() ; // goal 1
	glTranslated((5*dimension/2),0,-8) ;
	bb->drawHorizontal() ;
	glPopMatrix() ;
	glPushMatrix() ; // goal 2
	glTranslated((5*dimension/2),0,(5*dimension)+8) ;
	bb->drawHorizontal() ;
	glPopMatrix() ;

	// corners -----------------------------------------------
	// goal back left
	glPushMatrix() ;
	glTranslated((5*dimension/2)-3.5,0,-4.5) ;
	bb->drawVertical() ;
	glTranslated(0,0.5,-3.5) ;
	if( rounded == 1 )
		bb->drawCorner(1) ;
	else
		bb->drawCorner(0) ;
	glPopMatrix() ;

	// goal back right
	glPushMatrix() ;
	glTranslated((5*dimension/2)+3.5,0,-4.5) ;
	bb->drawVertical() ;
	glTranslated(0,0.5,-3.5) ;
	if( rounded == 1 )
		bb->drawCorner(2) ;
	else
		bb->drawCorner(0) ;
	glPopMatrix() ;

	// goal front left
	glPushMatrix() ;
	glTranslated((5*dimension/2)-3.5,0,5*dimension) ;
	glTranslated(0,0,4.5) ;
	bb->drawVertical() ;
	glTranslated(0,0.5,3.5) ;
	if( rounded == 1 )
		bb->drawCorner(3) ;
	else
		bb->drawCorner(0) ;
	glPopMatrix() ;

	// goal front right
	glPushMatrix() ;
	glTranslated((5*dimension/2)+3.5,0,5*dimension) ;
	glTranslated(0,0,4.5) ;
	bb->drawVertical() ;
	glTranslated(0,0.5,3.5) ;
	if( rounded == 1 )
		bb->drawCorner(4) ;
	else
		bb->drawCorner(0) ;
	glPopMatrix() ;
}
void Game::drawBoardBorders(int rounded)
{
	//board border ----------------------------------------------------
	int j=0 ;
	for( j=0 ; j<dimension ; j++ )
	{
		glPushMatrix() ; // back
		glTranslated(2.5,0,-1) ;
		glTranslated(5*j,0,0) ; // 5 is the width of a boardspot
		bb->drawHorizontal() ;
		glPopMatrix() ;
		glPushMatrix() ; // front
		glTranslated(2.5,0,-1) ;
		glTranslated(5*j,0,5*dimension+2) ;
		bb->drawHorizontal() ;
		glPopMatrix() ;
		glPushMatrix() ; // left
		glTranslated(-1,0,2.5) ;
		glTranslated(0,0,5*j) ;
		bb->drawVertical() ;
		glPopMatrix() ;
		glPushMatrix() ; // right
		glTranslated(-1,0,2.5) ;
		glTranslated(5*dimension+2,0,5*j) ;
		bb->drawVertical() ;
		glPopMatrix() ;
	}

	// board corners ----------------------------------------------------
	glPushMatrix() ; // back left
	glTranslated(-1,0.5,-1) ;
	if( rounded == 1 )
		bb->drawCorner(1) ;
	else
		bb->drawCorner(0) ;
	glPopMatrix() ;

	glPushMatrix() ; // back right
	glTranslated(5*dimension+2,0,0) ;
	glTranslated(-1,0.5,-1) ;
	if( rounded == 1 )
		bb->drawCorner(2) ;
	else
		bb->drawCorner(0) ;
	glPopMatrix() ;

	glPushMatrix() ; // front left
	glTranslated(0,0,5*dimension+2) ;
	glTranslated(-1,0.5,-1) ;
	if( rounded == 1 )
		bb->drawCorner(3) ;
	else
		bb->drawCorner(0) ;
	glPopMatrix() ;

	glPushMatrix() ; // front rigth
	glTranslated(5*dimension+2,0,5*dimension+2) ;
	glTranslated(-1,0.5,-1) ;
	if( rounded == 1 )
		bb->drawCorner(4) ;
	else
		bb->drawCorner(0) ;
	glPopMatrix() ;
}
void Game::drawPlayerTag()
{
	glPushMatrix() ; // player 1 north
	player1_tag->apply() ;
	glTranslated((dimension*5/4)-4,0.1,-5) ;
	glRotated(-90,1,0,0) ;
	glScaled(8,4,0.1) ;
	tag->drawface() ;
	glPopMatrix() ;

	glPushMatrix() ; // player 2 south
	player2_tag->apply() ;
	glTranslated((dimension*15/4)+4,0.1,(dimension*5)+5) ;
	glRotated(-90,1,0,0) ;
	glScaled(8,4,0.1) ;
	tag->drawface() ;
	glPopMatrix() ;
}
void Game::drawTime()
{
	unsigned long time = gameTime/1000;
	int hours = time/3600;
	int minutes = time/60 - hours*60;
	int seconds = time - (minutes*60 + hours*3600);
	char dH = '0' + hours/10;
	char uH = '0' + hours%10;
	char dM = '0' + minutes/10;
	char uM = '0' + minutes%10;
	char dS = '0' + seconds/10;
	char uS = '0' + seconds%10;

	glMatrixMode(GL_PROJECTION); // change the current matrix to PROJECTION
	double matrix[16]; // 16 doubles in stack memory
	glGetDoublev(GL_PROJECTION_MATRIX, matrix); // get the values from PROJECTION matrix to local variable
	glLoadIdentity(); // reset PROJECTION matrix to identity matrix
	glOrtho(0, 200, 0, 200, -5, 5); // orthographic perspective
	glMatrixMode(GL_MODELVIEW); // change current matrix to MODELVIEW matrix again
	glLoadIdentity(); // reset it to identity matrix
	glPushMatrix(); // push current state of MODELVIEW matrix to stack
	glLoadIdentity(); // reset it again. (may not be required, but it my convention)
	//glRasterPos2i(90,190); // raster position in 2D

	glPushMatrix();
	glDisable(GL_LIGHTING);
	glColor3f(0, 1, 0);
	glRasterPos2i(50,190);
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'T');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'i');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'm');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'e');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, ' ');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'p');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'l');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'a');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'y');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'e');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, 'd');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, ' ');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, ' ');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, dH);
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, uH);
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, ':');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, dM);
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, uM);
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, ':');
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, dS);
	glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, uS);




	glEnable(GL_LIGHTING);
	glPopMatrix();

	glPopMatrix(); // get MODELVIEW matrix value from stack
	glMatrixMode(GL_PROJECTION); // change current matrix mode to PROJECTION
	glLoadMatrixd(matrix); // reset
	glMatrixMode(GL_MODELVIEW); // change current matrix mode to MODELVIEW

}
void Game::drawPieces()
{
	int piece=0;
	glPushMatrix();
	glTranslated(-dimension*5/2,0,-dimension*5/2) ;
	for(unsigned int i=0 ; i<BoardSize ; i++ )
		for(unsigned int k=0 ; k<BoardSize ; k++ )
		{
			glPushMatrix() ;
			glTranslated(i*5 +2.5,1,k*5 +2.5) ;
			piece = tab->board[k][i];
			if(k==selectedYi && i==selectedXi) //highlight piece
			{
				switch(piece)
				{
				case 1:
					p1->draw(1);
					break;
				case 2:
					p2->draw(1);
					break;
				case 3:
					p3->draw(1);
					break;
				case 0:
					break;
				default:
					break;
				}
			}
			else
			{
				switch(piece)
				{
				case 1:
					p1->draw();
					break;
				case 2:
					p2->draw();
					break;
				case 3:
					p3->draw();
					break;
				case 0:
					break;
				default:
					break;
				}
			}
			glPopMatrix() ;
		}
		glPopMatrix();

		glPushMatrix() ;

		if( tab->activePlayer == 1 )
			glTranslated((dimension*5/4)+4,0,-(dimension*5/2)-4.5) ;
		else
			glTranslated(-(dimension*5/4)-4,0,(dimension*5/2)+4.5) ;

		switch(tab->suspendedPiece)
		{
		case 1:
			p1->draw();
			break;
		case 2:
			p2->draw();
			break;
		case 3:
			p3->draw();
			break;
		case 0:
			break;
		default:
			break;
		}
		glPopMatrix() ;
}
void Game::drawGoalPiece(int n)
{
	switch(n)
	{
	case 1:
		p1->draw();
		break;
	case 2:
		p2->draw();
		break;
	case 3:
		p3->draw();
		break;
	case 0:
		//draw nothing...how though?
		break;
	default:
		break;
	}
}
void Game::drawTextBox()
{
	glPushMatrix() ;
	if( tab->activePlayer == 1 ) 
		glRotated(180,0,1,0);

	glPushMatrix() ; // draw background
	glTranslated(0,10,0) ;
	glRotated(-45,1,0,0) ;
	newHs_tag->apply() ;
	glScalef(35,10,0.1) ;
	tag->drawface() ;
	glPopMatrix() ;

	glPushMatrix() ; // draw enter button
	glPushName(BACK) ;
	glTranslated(-5.4,4.2,4) ;
	glRotated(-45,1,0,0) ;
	enter_tag->apply() ;
	glScalef(10,4,0.1) ;
	tag->drawface() ;
	glPopName() ;
	glPopMatrix() ;

	glPushMatrix() ; // draw replay button
	glPushName(REPLAY) ;
	glTranslated(5.4,4.2,4) ;
	glRotated(-45,1,0,0) ;
	replay->apply() ;
	glScalef(10,4,0.1) ;
	tag->drawface() ;
	glPopName() ;
	glPopMatrix() ;

	glPushMatrix() ; // input name
	glTranslated(0,10,2) ;
	glRotated(-45,1,0,0) ;
	glScalef(0.02, 0.02, 0.02);
	charApp->apply() ;
	for( int i=0 ; i<playerName.size() ; i++ )
		glutStrokeCharacter(GLUT_STROKE_ROMAN , *playerName.substr(i,1).c_str() ) ;
	glPopMatrix() ;
	glPopMatrix() ;
}
void Game::drawBackButton()
{
	glPushMatrix() ;
	if( tab->activePlayer == 1 ) 
		glRotated(180,0,1,0);

	glPushMatrix() ; // draw enter button
	glPushName(BACK) ;
	glTranslated(-5.4,4.2,4) ;
	glRotated(-45,1,0,0) ;
	enter_tag->apply() ;
	glScalef(10,4,0.1) ;
	tag->drawface() ;
	glPopName() ;
	glPopMatrix() ;

	glPushMatrix() ; // draw replay button
	glPushName(REPLAY) ;
	glTranslated(5.4,4.2,4) ;
	glRotated(-45,1,0,0) ;
	replay->apply() ;
	glScalef(10,4,0.1) ;
	tag->drawface() ;
	glPopName() ;
	glPopMatrix() ;

	glPopMatrix();
}
void Game::movePiece(int Xi, int Yi,int Xf, int Yf)
{
	printf("MovePiece: %d %d %d %d\n",Xi,Yi,Xf,Yf);

	if( Xi==Xf && Yi==Yf )
	{
		printf("invalid move\n");
		selectedYi=-2 ; selectedXi=-2 ;
		return ;
	}

	if(Yf==6 || Yf==-1) //endGame move
	{
		if( FLAG_ONLY_JUMP )
		{
			if(tab->fimJogo(Xi,Yi,Xf,Yf,Plays->back()->getBonusPiece()))
			{
				GameEnded=true;
				printf("Player %d You won!\n",tab->activePlayer);
				Move* move;
				move = new Move(tab->activePlayer,Xi,Yi,Xf,Yf,tab->board[Yi][Xi],Plays->back()->getBonusPiece());
				Plays->push_back(move);
				tab->suspendedPiece=0;
				AnimatedMove(move);

				if( addInTopTen( hs , gameTime/1000 ) )
					FLAG_NewHS=true ;
			}
			else
			{
				printf("invalid end move\n");
				selectedXi=Xi;
				selectedYi=Yi;
				pieceSelected=true;
				return;
			}
		}
		else
		{
			if(tab->fimJogo(Xi,Yi,Xf,Yf,tab->board[Yi][Xi]))
			{
				GameEnded=true;
				printf("Player %d You won!\n",tab->activePlayer);
				Move* move;
				move = new Move(tab->activePlayer,Xi,Yi,Xf,Yf,tab->board[Yi][Xi],Plays->back()->getEndPiece());
				Plays->push_back(move);
				tab->suspendedPiece=0;
				AnimatedMove(move);

				if( addInTopTen( hs , gameTime/1000 ) )
					FLAG_NewHS=true ;
			}
			else
			{
				printf("invalid move\n");
				selectedXi=Xi;
				selectedYi=Yi;
				pieceSelected=true;
				return;
			}
		}
	}
	else
	{

		if(tab->suspendedPiece!=0)
		{
			//executebonus move!
			if(tab->bonusMove(Xi,Yi,Xf,Yf,Plays->back()->getBonusPiece(),bonusInUse))
			{	
				if(tab->board[Yf][Xf]!=0) //bonus move!
				{
					//cout << "\n\nthis other case where you got bonus!\n\n";

					FLAG_ONLY_JUMP = 1 ;

					Move* move= new Move(tab->activePlayer,Xi,Yi,Xf,Yf,tab->board[Yi][Xi],Plays->back()->getBonusPiece(),tab->board[Yf][Xf]);
					Plays->push_back(move);
					selectedXi=selectedXf;
					selectedYi=selectedYf;
					tab->suspendedPiece=tab->board[Yf][Xf];
					pieceSelected=true;
					AnimatedMove(move);
				}
				else //done player move
				{
					//cout << "\n>>>>>>>>>>>>>>>>>> THIS CASE!<<<<<<<<<<<<<<<<<<\n\n";
					Move* move ;

					if( this->bonusInUse == BONUS_SWITCH )
						move= new Move(tab->activePlayer,Xi,Yi,Xf,Yf,Plays->back()->getBonusPiece(),tab->board[Yi][Xi]);
					if( this->bonusInUse == BONUS_JUMP )				
						move= new Move(tab->activePlayer,Xi,Yi,Xf,Yf,tab->board[Yi][Xi],Plays->back()->getBonusPiece());

					Plays->push_back(move);
					if(tab->activePlayer==1)
						tab->activePlayer=2;
					else
						tab->activePlayer=1;

					this->bonusInUse = BONUS_JUMP ;
					FLAG_ONLY_JUMP = 0 ;

					tab->suspendedPiece=0;
					AnimatedMove(move);
					//cout << endl << "end " << endl;
					//tab->board[Yi][Xi]=0;
				}

			}
			else
			{
				selectedXi=Xi;
				selectedYi=Yi;
				pieceSelected=true;
				printf("Invalid bonus play!\n");
			}
		}
		else
		{
			if(tab->validateMove(Xi,Yi,Xf,Yf))
			{	
				if(tab->board[Yf][Xf]!=0) //bonus move!
				{
					//cout << "\n================== you got bonus!===================\n\n";

					Move* move= new Move(tab->activePlayer,Xi,Yi,Xf,Yf,tab->board[Yi][Xi],0,tab->board[Yf][Xf]);

					Plays->push_back(move);
					selectedXi=selectedXf;
					selectedYi=selectedYf;
					tab->suspendedPiece=tab->board[Yf][Xf];
					pieceSelected=true;
					AnimatedMove(move);

				}
				else //done player move
				{
					//cout << "last case!\n";
					Move* move= new Move(tab->activePlayer,Xi,Yi,Xf,Yf,tab->board[Yi][Xi],0);
					Plays->push_back(move);
					if(tab->activePlayer==1)
						tab->activePlayer=2;
					else
						tab->activePlayer=1;
					tab->suspendedPiece=0;
					AnimatedMove(move);
				}

			}
			else
			{
				printf("Invalid play!\n");
				selectedYi=-2 ; selectedXi=-2 ;
				return;
			}
		}
	}
}
void Game::drawMovie()
{

}
void Game::AnimatedMove(Move* m)
{
	BoardPiece* aux;
	m->print();
	switch(m->getEndPiece())
	{
	case 1:
		aux=p1;
		break;
	case 2: 
		aux=p2;
		break;
	case 3:
		aux=p3;
		break;
	default:
		return;
	}
	if(!GameEnded || MOVIE_FLAG)
		anime->StartAnimation(aux, m->getStart().first, m->getStart().second, m->getFinish().first, m->getFinish().second, m->getEndPiece());
	else
		anime->startEndMoveAnimation(aux,m->getStart().first, m->getStart().second, m->getFinish().second, m->getStartPiece(), m->getPlayer());

	tab->board[m->getStart().second][m->getStart().first]=m->getStartPiece();
}

void Game::changeView()
{
	//cout << "entered\n";
	camRotation=true;
	//cout << activeCamera->rotateTo(CG_CGFcamera_AXIS_Y,180,60);
	//cout << "exit\n";
}

void Game::startMovieAnimation()
{
	printf("ROLING MOVIE\n");
	movieIndex=0;
	Goal1=0;
	Goal2=0;
	tab->reset();
	AnimatedMove(Plays->at(movieIndex));
	movieIndex++;
}

bool Game::nextMovieMove()
{
	if(movieIndex>=Plays->size())
		return true;
	else if(movieIndex>=Plays->size()-1)
	{	
		MOVIE_FLAG=false;
		AnimatedMove(Plays->at(movieIndex));
		return false; //done movie
	}
	AnimatedMove(Plays->at(movieIndex));
	movieIndex++;
	return false;
}

void Game::IAMove()
{

}

void Game::startServer()
{
	STARTUPINFO si;

	ZeroMemory( &si, sizeof(si) );
	si.cb = sizeof(si);
	ZeroMemory( &pi, sizeof(pi) );

	// Start the child process. 
	if( !CreateProcess( NULL,   // No module name (use command line)
		TEXT("gyges.exe"),        // Command line
		NULL,           // Process handle not inheritable
		NULL,           // Thread handle not inheritable
		FALSE,          // Set handle inheritance to FALSE
		0,              // No creation flags
		NULL,           // Use parent's environment block
		NULL,           // Use parent's starting directory 
		&si,            // Pointer to STARTUPINFO structure
		&pi )           // Pointer to PROCESS_INFORMATION structure
		) 
	{
		printf( "CreateProcess failed (%d).\n", GetLastError() );
		return;
	}
}

void Game::closeServer()
{
	// Wait until child process exits.
	WaitForSingleObject( pi.hProcess, INFINITE );

	// Close process and thread handles. 
	CloseHandle( pi.hProcess );
	CloseHandle( pi.hThread );
}


void Game::setAppearance( int App )
{
	bb->setAppearance( App ) ;
	bs->setAppearance( App ) ;
	p1->setAppearance( App ) ;
	p2->setAppearance( App ) ;
	p3->setAppearance( App ) ;

	FLAG_APPCHANGE = false ;
}