#ifndef CONFMENU_H
#define CONFMENU_H

#include "BoardPieces.h"
#include "Scene.h"
#include "CGFlight.h"

class ConfMenu: public Scene {
private:
	UnitCube * board ;
	CGFappearance * back_tab ;
	CGFappearance * conf_tab ;
	CGFappearance * background ;
	CGFappearance * symbol ;
	CGFappearance * checked ;
	CGFappearance * unchecked ;
	int option1 , option2 , option3 , option4 ;
	CGFappearance * characters ;
	camera * initialCamera ;
public:
	ConfMenu(int op1=DIFF_EASY, int op2=MODE_PvP , int op3=BORDER_RECT , int op4=BG_WOOD);
	virtual ~ConfMenu();
	void setOption1(int op){ option1 = op ; this->Dificulty=op;}
	void setOption2(int op){ option2 = op ; this->GameType=op;}
	void setOption3(int op){ option3 = op ; this->BoardDraw=op;}
	void setOption4(int op){ option4 = op ; this->Background=op;}

	void display();
	void drawDifficulty(); // op1
	void drawGameMode(); // op2
	void drawBoardMode(); // op3
	void drawBackGroundMode(); // op4
	void drawCheckBox( int check );

	void update(unsigned long t);
	int pickedValue(int Value,int parentCalled=1);
	void keyPressed(char key){};
	void init();
	void leave();
	string getName(){return "ConfMenu";}
};

#endif