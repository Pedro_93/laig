#ifndef TABULEIRO_H
#define TABULEIRO_H 

#include <string>
#include <sstream>
#include "mySocket.h"
#include <stdlib.h>     /* atoi */

using namespace std;

class Tabuleiro
{
	int size;
	mySocket* sock;
public:
	int activePlayer;
	int suspendedPiece;
	int** board;
	Tabuleiro(int tam);
	bool start();
	void quit();
	string toStr();
	void fromStr(char* strTab);
	void setUp(int option);
	void setPiece(int X, int Y, int Value);
	int getPiece(int X, int Y);
	bool validateMove(int Xi, int Yi, int Xf, int Yf);
	bool bonusMove(int Xi,int Yi,int Xf,int Yf,int bonus,int bonusType);
	bool fimJogo(int Xi, int Yi, int Xf, int Yf,int piece);
	void print();
	void computerPlay();
	int getSize() { return this->size ; }
	void reset();
	~Tabuleiro();
};
#endif