#ifndef GYGES_H
#define GYGES_H
#include "Menu.h"
#include "ConfMenu.h"
#include "HSMenu.h"
#include "Game.h"
#include "Shared.h"
#include <iostream>

class Gyges: public CGFscene
{
private:
	Scene* currentScene;
	Scene* states[4];
	int ActiveState;
public:
	Gyges();
	~Gyges(void);
	void changeState(int valor);
	void init();
	void update(unsigned long millis);
	void display();
	void pickedValue(int valor);
	void keyPressed(char key);
};

#endif