#include "mySocket.h"

#define BONUS_JUMP		1
#define BONUS_SWITCH	2

using namespace std;

static char* answer = new char[255];

mySocket::~mySocket(void)
{}

bool mySocket::startConnection() {// Initialize Winsock.
	WSADATA wsaData;
	int iResult = WSAStartup(MAKEWORD(2,2), &wsaData);
	if (iResult != NO_ERROR)
		printf("Client: Error at WSAStartup().\n");
	else
		printf("Client: WSAStartup() is OK.\n");

	// Create a socket.
	m_socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (m_socket == INVALID_SOCKET) {
		printf("Client: socket() - Error at socket(): %ld\n", WSAGetLastError());
		WSACleanup();
		return false;
	}
	else
		printf("Client: socket() is OK.\n");

	// Connect to a server.
	sockaddr_in clientService;
	clientService.sin_family = AF_INET;
	// Just test using the localhost, you can try other IP address
	clientService.sin_addr.s_addr = inet_addr("127.0.0.1");
	clientService.sin_port = htons(60070);

	if (connect(m_socket, (SOCKADDR*)&clientService, sizeof(clientService)) == SOCKET_ERROR) {
		printf("Client: connect() - Failed to connect.\n");
		WSACleanup();
		return false;
	}
	else {
		printf("Client: connect() is OK.\n");
		printf("Client: Can start sending and receiving data...\n");
	}

	// Send and receive data.
	printf("Connected\n");
	return true;
}

void mySocket::envia(char *s, int len) {
	int bytesSent = send(m_socket, s, len, 0);
	if(bytesSent == SOCKET_ERROR)
	{printf("Client: send() error %ld.\n", WSAGetLastError());return;}
}

void mySocket::recebe(char *ans) {
	int bytesRecv = SOCKET_ERROR;
	int pos = 0;
	while (true) {
		recv(m_socket, &ans[pos], 1, 0);
		if (ans[pos] == '\n')
			break;
		pos++;
	}
	ans[pos] = 0;
	//cout << "prolog answered: " << ans << endl;
}

char * mySocket::criaTabuleiro(int size){
	stringstream x;
	x<<"initialize(" << size << ").\n";

	char *snd = new char[x.str().length() + 1];
	strcpy(snd, x.str().c_str());

	envia(snd, strlen(snd));
	recebe(answer);

	delete [] snd;

	return answer;
}

char* mySocket::setPeca(string tab,int X,int Y, int Value)
{
	stringstream x;
	x<<"setPeca(" << tab <<","<<X << "," <<Y<< ","<< Value<<").\n";

	char *snd = new char[x.str().length() + 1];
	strcpy(snd, x.str().c_str());

	envia(snd, strlen(snd));
	recebe(answer);

	delete [] snd;

	return answer;
}

//jogadaValida(Tab,N,[X,Y],[PosX,PosY],TipoPeca):-
int mySocket::validateMove(string tab,int size, int Xi, int Yi, int Xf, int Yf,int Value, int activePlayer)
{
	stringstream x;
	x<<"validate(" << tab <<","<< size << "," <<Xi << "," <<Yi<< ","<<Xf << "," <<Yf<< ","<< Value<<","<<activePlayer<<").\n";
	char *snd = new char[x.str().length() + 1];
	strcpy(snd, x.str().c_str());
	envia(snd, strlen(snd));
	recebe(answer);

	delete [] snd;
	if(answer=="invalid.")
	{
		return -1;
	}
	else
	{
		return atoi(answer);
	}
}

int mySocket::validateBonusMove(string tab,int size, int Xi, int Yi, int Xf, int Yf,int Value, int bonusType, int activePlayer)
{
	stringstream x;

	if( bonusType == BONUS_JUMP )
			x<<"validateBonusJump(" << tab <<","<< size << "," <<Xi << "," <<Yi<< ","<<Xf << "," <<Yf<< ","<< Value<<","<<activePlayer<<").\n";
	else if ( bonusType == BONUS_SWITCH )
			x<<"validateBonusSwitch(" << tab <<","<< Xf << "," <<Yf<< ","<<activePlayer<<").\n";

	char *snd = new char[x.str().length() + 1];
	strcpy(snd, x.str().c_str());
	envia(snd, strlen(snd));
	recebe(answer);

	delete [] snd;
	if(answer=="invalid.")
	{
		return -1;
	}
	else
	{
		return atoi(answer);
	}
}

int mySocket::validateEndGame(std::string tab, int Xi, int Yi, int Xf, int Yf,int Value,int Player)
{
	stringstream x;
	x<<"fimJogo(" << tab << "," <<Xi << "," <<Yi<< ","<<Xf << "," <<Yf<< ","<< Value<<","<< Player <<").\n";
	char *snd = new char[x.str().length() + 1];
	strcpy(snd, x.str().c_str());
	envia(snd, strlen(snd));
	recebe(answer);

	delete [] snd;
	if(answer=="invalid.")
	{
		return -1;
	}
	else
	{
		return atoi(answer);
	}
}

void mySocket::quit() {
	cout << "Asking prolog to quit" << endl;
	char buff[] = "quit.\n";
	envia(buff, 6);
	recebe(answer);
}