#ifndef SHARED_H
#define SHARED_H

#include <deque>
#include <iostream>

/* ---------------------------------------------------- */
/*					  Board draw mode					*/
#define BORDER_ROUNDED	11
#define	BORDER_RECT		12

/* ---------------------------------------------------- */
/*				   Checkbox true or false				*/
#define BOX_CHECK		10
#define	BOX_UNCHECK		20

/* ---------------------------------------------------- */
/*			    Config menu available options			*/
#define DIFF_EASY		5
#define DIFF_MEDIUM		6
#define DIFF_HARD		7
#define MODE_PvP		8
#define MODE_PvPC		9
#define MODE_PCvPC		10
#define BG_WOOD			13
#define BG_WATER		14
#define BG_COLORFUL		15

/* ---------------------------------------------------- */
/*					   Bonus Moves						*/
#define BONUS_JUMP		1
#define BONUS_SWITCH	2

/* ---------------------------------------------------- */
/*						  lights						*/

/* ---------------------------------------------------- */
/*						appearances						*/

class Move{
	int player,bonusPiece,endPiece,startPiece;
	std::pair<pair<int,int>,pair<int,int>> position;
public:
	Move()
	{
		player=0;
		endPiece=0;
		startPiece=0;
		bonusPiece=0;
		position=std::make_pair(std::make_pair(0,0),std::make_pair(1,1));
	}
	Move(int p,int Xi,int Yi,int Xf,int Yf,int Endpiece,int starPiece, int bonus=0)
	{
		endPiece=Endpiece;
		startPiece=starPiece;
		player=p;
		bonusPiece=bonus;
		position=std::make_pair(std::make_pair(Xi,Yi),std::make_pair(Xf,Yf));
	}

	pair<int,int>& getStart()
	{
		return position.first;
	}
	
	pair<int,int>& getFinish()
	{
		return position.second;
	}

	int getPlayer()
	{
		return player;
	}

	int getStartPiece()
	{
		return startPiece;
	}

	int getEndPiece()
	{
		return endPiece;
	}

	void print()
	{
		cout <<  "Player "<<player<<": Moving piece: "<<endPiece <<" from ("<<position.first.first<<","<<position.first.second<<") to ("<<position.second.first<<","<<position.second.second<<") putting " << startPiece <<" at origin";
		if(bonusPiece!=0)
			cout << ", bonus: " << bonusPiece;
		cout << endl;
	}

	int getBonusPiece(){return bonusPiece;}
};

static std::deque<Move*>* Plays = new std::deque<Move*>();

#endif