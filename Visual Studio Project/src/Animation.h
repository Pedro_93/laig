#ifndef ANIMATION_H
#define ANIMATION_H
#include <math.h>       /* sin */
#include "Tabuleiro.h"
#include "BoardPieces.h"
#include <stdlib.h>     /* abs */
#include <iostream>
#define PI 3.14159265359

class Animation
{
protected:
	unsigned long duration;
	unsigned long startTime;
	unsigned long endTime;
	unsigned char doReset;
	bool done,active;
	const int dimension ;
	void init(unsigned long t);
public:
	Animation(unsigned long du,int d):dimension(d){duration=du;done=false;}
	virtual int update(unsigned long t)=0;
	virtual void draw()=0;
	virtual void StartAnimation(BoardPiece*p,int Xi, int Zi, int Xf, int Zf, int EP)=0;
	virtual void startEndMoveAnimation(BoardPiece*p,int Xi, int Zi, int Zf, int EP, int player)=0;
	virtual bool isDone()=0;
	virtual bool isActive()=0;
};

class PieceAnimation:public Animation
{
private:
	int winnerSide;
	Tabuleiro* tab;
	BoardPiece* piece;
	point startPos;
	point MidPos;
	point endPos;
	signed double xInc,zInc,yInc;
	unsigned double MaxY;
	int xi,zi,xf,zf,EndP;
public:
	PieceAnimation(Tabuleiro*ta,unsigned long duration,int dimension, unsigned double maxY):Animation(duration,dimension){
		tab=ta;
		this->MaxY=maxY;
		active=false;
	}

	void StartAnimation(BoardPiece*p,int Xi, int Zi, int Xf, int Zf, int EP)
	{
		active=true;
		this->doReset=true;
		this->xi=Xi;
		this->zi=Zi;
		this->xf=Xf;
		this->zf=Zf;
		this->EndP=EP;
		this->done=false;
		piece=p;
		MidPos = point();
		startPos = point(Xi,0,Zi);
		endPos= point(Xf,0,Zf);
		xInc=((Xf-Xi)*5);
		zInc=((Zf-Zi)*5);
		printf("(%d,%d) ------- (%d,%d): %d\n",Xi,Zi,Xf,Zf,EP);
// 		cout << "start: (" << startPos.ctrlpoint[0] << "," << startPos.ctrlpoint[1] << "," << startPos.ctrlpoint[2]<<")\n";
// 		cout << "end: (" << endPos.ctrlpoint[0] << "," << endPos.ctrlpoint[1] << "," << endPos.ctrlpoint[2]<<")\n";
// 		cout << "time: " << duration << ", DeltaX: "<< xInc << ", DeltaZ: " <<zInc << endl; 
	}

	void startEndMoveAnimation(BoardPiece*p,int Xi, int Zi, int Zf, int EP, int player)
	{
		winnerSide=player;
		active=true;
		doReset=true;
		this->xi=Xi;
		this->zi=Zi;
		this->zf=Zf;
		this->done=false;
		piece=p;
		MidPos=point();
		startPos=point(Xi,0,Zi);
		
		float Xf=(tab->getSize() /2)-0.4;

		if( player == 2 )
		{
			zInc=(((Zf-0.5)-zi)*5);
		}
		else if ( player == 1 )
		{
			zInc=(((Zf+0.5)-zi)*5);
		}
		xInc=((Xf-xi)*5);
	}

	int update(unsigned long t);
	void draw();
	bool isDone(){return !active;}
	bool isActive(){
		return active;
	}
};

#endif