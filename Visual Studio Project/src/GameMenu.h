#ifndef GAMEMENU_H
#define GAMEMENU_H

#include "BoardPieces.h"

class GameMenu {
private:
	UnitCube * board ;
	CGFappearance * ng_tab ;
	CGFappearance * exit_tab ;
	CGFappearance * hs_tab ;
	CGFappearance * config_tab ;
	CGFappearance * background ;
	CGFappearance * symbol ;
public:
	GameMenu()
	{
		this->board = new UnitCube(1,1) ;
		this->ng_tab = new CGFappearance("../data/game_menu/new_game.jpg",1.0,1.0) ;
		this->exit_tab = new CGFappearance("../data/game_menu/exit.jpg",1.0,1.0) ;
		this->hs_tab = new CGFappearance("../data/game_menu/hs.jpg",1.0,1.0) ;
		this->background = new CGFappearance("../data/game_menu/background.jpg",15.0,8.0) ;
		this->symbol = new CGFappearance("../data/game_menu/gyges.jpg",1.0,1.0) ;
		this->config_tab = new CGFappearance("../data/game_menu/config.jpg" , 1.0,1.0) ;
	}

	virtual ~GameMenu()
	{
		delete(board) ;
		delete(exit_tab) ;
		delete(hs_tab) ;
		delete(config_tab) ;
		delete(background) ;
		delete(symbol) ;
	}

	void draw()
	{
		glPushMatrix() ;

		glTranslatef(4,0,0) ;

		// new game
		glPushMatrix() ;
			glPushName(1) ;
			ng_tab->apply();
			glTranslatef(-10,-2.5,0.55) ;
			glScalef(8,2.0,0.1) ;
			board->drawface() ;
			glPopName() ;
		glPopMatrix() ;

		// highscore
		glPushMatrix() ;
			glPushName(2) ;
			hs_tab->apply();
			glTranslatef(2,-2.5,0.55) ;
			glScalef(8,2.0,0.1) ;
			board->drawface() ;
			glPopName() ;
		glPopMatrix() ;

		// config
		glPushMatrix() ;
			glPushName(3) ;
			config_tab->apply() ;
			glTranslatef(2,-4.8,0.55) ;
			glScalef(8,2.0,0.1) ;
			board->drawface() ;
			glPopName() ;
		glPopMatrix() ;

		// exit
		glPushMatrix() ;
			glPushName(4) ;
			exit_tab->apply();
			glTranslatef(-10,-4.8,0.55) ;
			glScalef(8,2.0,0.1) ;
			board->drawface() ;
			glPopName() ;
		glPopMatrix() ;

		glPopMatrix() ;

		// background
		glPushMatrix() ;
			background->apply() ;
			glScalef(25.0,25.0,0.1) ;
			board->draw() ;
		glPopMatrix() ;

		// symbol
		glPushMatrix() ;
			symbol->apply() ;
			glTranslatef(0,3,0);
			glScalef(20.0,6.0,0.1) ;
			board->draw() ;
		glPopMatrix() ;
	}
} ;

#endif