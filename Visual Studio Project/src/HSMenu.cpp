#include "HSMenu.h"



HSMenu::HSMenu()
{
	this->board = new UnitCube() ;
	this->back_tab = new CGFappearance("../data/game_menu/back.jpg",1.0,1.0) ;
	this->background = new CGFappearance("../data/game_menu/background.jpg",15.0,8.0) ;
	this->symbol = new CGFappearance("../data/game_menu/symbol.jpg",1.0,1.0) ;
	this->hs_tab = new CGFappearance("../data/game_menu/hs_menu.jpg" , 1.0,1.0) ;
	this->charApp = new CGFappearance("../data/solid/white.jpg" , 1.0 , 1.0) ;

	creatTextFile(HSFilename) ;
	hs = readHighScores(HSFilename) ;
}

HSMenu::~HSMenu()
{
	delete(board) ;
	delete(back_tab) ;
	delete(hs_tab) ;
	delete(background) ;
	delete(symbol) ;
}

void HSMenu::init(){
	initialCamera = new ortho("otho",0.0,100.0,-25.0,25.0,25.0,-25.0) ;
}

void HSMenu::leave(){
	delete(initialCamera);
}

void HSMenu::update(unsigned long t){}

void HSMenu::display()
{
	initialCamera->applyView();
	glPushMatrix();
	this->drawHS() ;
	glPopMatrix() ;

	// hs
	glPushMatrix() ;
	hs_tab->apply() ;
	glTranslatef(-6.5,4.5,0.55) ;
	glScalef(8,2.0,0.1) ;
	board->drawface() ;
	glPopMatrix() ;

	// back tab
	glPushMatrix() ;
	glPushName(0) ;
	back_tab->apply();
	glTranslated(-6.5,-6.0,0.55) ;
	glScalef(4,1.0,0.1) ;
	board->drawface() ;
	glPopName() ;
	glPopMatrix() ;

	// symbol
	glPushMatrix() ;
	symbol->apply() ;
	glTranslatef(-6.5,-1,0.1);
	glScalef(6.0,6.0,0.1) ;
	board->draw() ;
	glPopMatrix() ;

	// background
	glPushMatrix() ;
	background->apply() ;
	glScalef(25.0,25.0,0.1) ;
	board->draw() ;
	glPopMatrix() ;
}

void HSMenu::drawHS()
{
	int pos = 0 ;
	glPushMatrix() ;
	glTranslatef(2,3,0) ;

	glPushMatrix() ;
	charApp->apply() ;
	glTranslated(-4,1.5,0.55) ;
	glScalef(0.005, 0.005, 0.005);
	glColor3f(1.0,1.0,1.0);
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'P' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'o' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 's' ) ;
	glPopMatrix() ;
	glPushMatrix() ;
	glTranslated(0,1.5,0.55) ;
	glScalef(0.005, 0.005, 0.005);
	glColor3f(1.0,1.0,1.0);
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'T' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'i' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'm' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'e' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , '(' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 's' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , ')' ) ;
	glPopMatrix() ;
	glPushMatrix() ;
	glTranslatef(5,1.5,0.55);
	glScalef(0.005, 0.005, 0.005);
	glColor3f(1.0,1.0,1.0);
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'P' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'l' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'a' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'y' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'e' ) ;
	glutStrokeCharacter(GLUT_STROKE_ROMAN , 'r' ) ;
	glPopMatrix() ;


	int counter = 1 ;
	for( HighScore::iterator it = hs->begin() ; it !=hs->end() ; it++ )
	{
		if( counter == 11 )
			break ;

		glPushMatrix() ;
		
		glTranslated(0,pos,0) ;

		ostringstream convert ;		convert<<it->first ;
		string result ;				result = convert.str() ;

		glPushMatrix() ;
		charApp->apply() ;
		char posChar[3] ;
		itoa( counter , posChar , 10 ) ;

		glTranslatef(-3.8,0,1);
		glScalef(0.005, 0.005, 0.005);
		glColor3f(1.0,1.0,1.0);
		if( counter < 10 )
			glutStrokeCharacter(GLUT_STROKE_ROMAN , posChar[0]) ;
		else
		{
			glutStrokeCharacter(GLUT_STROKE_ROMAN , posChar[0]) ;
			glutStrokeCharacter(GLUT_STROKE_ROMAN , posChar[1]) ;
		}
		counter+=1 ;
		glPopMatrix() ;

		int i=0 ;
		// time
		glPushMatrix() ;
		glTranslatef(0,0,1);
		glScalef(0.005, 0.005, 0.005);
		glColor3f(1.0,1.0,1.0);
		charApp->apply() ;
		for( ; i<result.length() ; i++ )
			glutStrokeCharacter(GLUT_STROKE_ROMAN , *result.substr(i,1).c_str() ) ;
		glPopMatrix() ;

		// nomes dos jogadores
		glPushMatrix() ;
		glTranslatef(4.9,0,1);
		glScalef(0.005, 0.005, 0.005);
		glColor3f(1.0,1.0,1.0);
		charApp->apply() ;
		for( i=0 ; i<it->second.length() ; i++ )
			glutStrokeCharacter(GLUT_STROKE_ROMAN , *it->second.substr(i,1).c_str() ) ;
		glPopMatrix() ;

		glPopMatrix() ;

		pos-=1 ;
	}

	glPopMatrix() ;
}

int HSMenu::pickedValue(int Value,int parentCalled){
	if(Value==BACK)
		return BACK;
	else
		return HIGH;
}

void HSMenu::keyPressed(char key){}