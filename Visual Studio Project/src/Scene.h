#ifndef SCENE_H 
#define SCENE_H 

#include "CGFscene.h"
#include "GameCam.h"
#include "FileManipulation.h"
#include "Shared.h"

#define BACK 0
#define MENU 0
#define PLAY 1
#define HIGH 2
#define CONFIG 3
#define EXIT 4

class Scene: public CGFscene
{
public:
	static HighScore * hs ;
	
	static int GameType;
	static int Dificulty;
	static int BoardDraw;
	static int Background;

	static bool FLAG_APPCHANGE ;

	virtual void display()=0;
	virtual void update(unsigned long t)=0;
	virtual int pickedValue(int Value,int parentCalled=1)=0;
	virtual void keyPressed(char key)=0;
	virtual void init()=0;
	virtual void leave()=0;
	virtual string getName()=0;
};

#endif