#ifndef BOARDPIECES_H
#define BOARDPIECES_H

#include "Primitives.h"
#include "Shared.h"

////////////////////////////////////////////////////////////////////////
////////////				Board Pieces					////////////
////////////////////////////////////////////////////////////////////////


class BoardPiece {
protected:
	CGFappearance * pieceTex ;		/* textura a aplicar nas peças */
	CGFappearance * pieceTexSel ;

	float BOARD_AMBIENT[4] ;
	float BOARD_SPECULAR[4];
	float BOARD_DIFFUSE[4];

	Cylinder * cylinder ;
	float textS , textT ;
	static int counter ;
	const int id ;
public:
	BoardPiece( string texture , string sel , int type , float text_s=0 , float text_t=0 ): id(counter++)
	{
		BOARD_AMBIENT[0]=0.5 ; BOARD_AMBIENT[1]=0.5 ; BOARD_AMBIENT[2]=0.5 ; BOARD_AMBIENT[3]=0.1 ;
		BOARD_SPECULAR[0]=0 ; BOARD_SPECULAR[1]=0 ; BOARD_SPECULAR[2]=0 ; BOARD_SPECULAR[3]=0 ;
		BOARD_DIFFUSE[0]=0.8; BOARD_DIFFUSE[1]=0.8; BOARD_DIFFUSE[2]=0.8; BOARD_DIFFUSE[3]=1.0 ;

		this->pieceTex = new CGFappearance(texture,GL_REPEAT, GL_REPEAT);
		pieceTex->setAmbient(BOARD_AMBIENT) ;
		pieceTex->setDiffuse(BOARD_DIFFUSE) ;
		pieceTex->setSpecular(BOARD_SPECULAR) ;
		this->pieceTexSel = new CGFappearance( sel , GL_REPEAT , GL_REPEAT ) ;
		pieceTexSel->setAmbient(BOARD_AMBIENT) ;
		pieceTexSel->setDiffuse(BOARD_DIFFUSE) ;
		pieceTexSel->setSpecular(BOARD_SPECULAR) ;

		if( text_s != 0 && text_t != 0 )
		{
			this->textS = text_s ;
			this->textT = text_t ;
		}
		else
		{
			this->textS = 1.0f ;
			this->textT = 1.0f ;
		}
		this->cylinder = new Cylinder( 1.0 , 1.0 , 1.0 , 30 , 30 , this->textS , this->textT ) ;
	}
	virtual ~BoardPiece() 
	{
		delete(cylinder);
		delete(pieceTex);
	}
	virtual void draw(int sel=0)=0 ;
	void draw(int x,int z)
	{
		glPushMatrix() ;
			glTranslated( x*5 , 0 , z*5 ) ;
			this->draw() ;
		glPopMatrix() ;
	}
	
	int getID() { return this->id ; }

	void setAppearance( int App )
	{
		pieceTex->~CGFappearance() ;
		pieceTexSel->~CGFappearance() ;

		switch( App )
		{
		case BG_WOOD :
			pieceTex = new CGFappearance("../data/wood/wood_reflex.jpg",1,1) ;
			pieceTex->setAmbient(BOARD_AMBIENT) ;
			pieceTex->setDiffuse(BOARD_DIFFUSE) ;
			pieceTex->setSpecular(BOARD_SPECULAR) ;
			pieceTexSel = new CGFappearance("../data/wood/wood_reflex_red.jpg",1,1) ;
			pieceTexSel->setAmbient(BOARD_AMBIENT) ;
			pieceTexSel->setDiffuse(BOARD_DIFFUSE) ;
			pieceTexSel->setSpecular(BOARD_SPECULAR) ;
			break ;
		case BG_WATER :
			pieceTex = new CGFappearance("../data/water/water.jpg",1,1) ;
			pieceTex->setAmbient(BOARD_AMBIENT) ;
			pieceTex->setDiffuse(BOARD_DIFFUSE) ;
			pieceTex->setSpecular(BOARD_SPECULAR) ;
			pieceTexSel = new CGFappearance("../data/water/water_red.jpg",1,1) ;
			pieceTexSel->setAmbient(BOARD_AMBIENT) ;
			pieceTexSel->setDiffuse(BOARD_DIFFUSE) ;
			pieceTexSel->setSpecular(BOARD_SPECULAR) ;
			break ;
		case BG_COLORFUL :
			pieceTex = new CGFappearance("../data/solid/pink.jpg" , 1 , 1) ;
			pieceTex->setAmbient(BOARD_AMBIENT) ;
			pieceTex->setDiffuse(BOARD_DIFFUSE) ;
			pieceTex->setSpecular(BOARD_SPECULAR) ;
			pieceTexSel = new CGFappearance("../data/solid/red.jpg" , 1 , 1 ) ;
			pieceTexSel->setAmbient(BOARD_AMBIENT) ;
			pieceTexSel->setDiffuse(BOARD_DIFFUSE) ;
			pieceTexSel->setSpecular(BOARD_SPECULAR) ;
			break ;
		}
	}
} ;

class SinglePiece : public BoardPiece {
private:
public:
	SinglePiece( string texture, string sel , float s=0, float t=0 ): BoardPiece(texture,sel,s,t){}
	virtual void draw(int sel=0)
	{
		glPushMatrix() ;
		//glPushName(this->getID()) ;
		glRotatef(-90.0,1.0,0.0,0.0) ;
		if( sel == 0 )
			this->pieceTex->apply() ;
		else
			pieceTexSel->apply() ;

		glPushMatrix() ;
		glScaled( 2.0 , 2.0 , 1.0 ) ;
		this->cylinder->draw() ;
		glPopMatrix() ;

		//glPopName() ;
		glPopMatrix() ;
	}
} ;

class DoublePiece : public BoardPiece {
private:
public:
	DoublePiece( string texture, string sel, float s=0, float t=0 ): BoardPiece(texture,sel,s,t){}
	virtual void draw(int sel=0)
	{
		glPushMatrix() ;
		//glPushName(this->getID()) ;
		glRotatef(-90.0,1.0,0.0,0.0) ;
		if( sel == 0 )
			this->pieceTex->apply() ;
		else
			pieceTexSel->apply() ;

		// bottom
		glPushMatrix() ;
		glScaled( 2.0 , 2.0 , 1.0 ) ;
		this->cylinder->draw() ;
		glPopMatrix() ;

		// top
		glPushMatrix() ;
		glTranslated( 0.0 , 0.0 , 1.0 ) ;
		glScaled( 1.5 , 1.5 , 1.0 ) ;
		this->cylinder->draw() ;
		glPopMatrix() ;

		//glPopName() ;
		glPopMatrix() ;
	}
} ;

class TriplePiece : public BoardPiece {
private:
public:
	TriplePiece( string texture, string sel, float s=0, float t=0 ): BoardPiece(texture,sel,s,t){}
	virtual void draw(int sel=0)
	{
		glPushMatrix() ;
		//glPushName(this->getID()) ;
		glRotatef(-90.0,1.0,0.0,0.0) ;
		if( sel == 0 )
			this->pieceTex->apply() ;
		else
			pieceTexSel->apply() ;

		// bottom
		glPushMatrix() ;
		glScaled( 2.0 , 2.0 , 1.0 ) ;
		this->cylinder->draw() ;
		glPopMatrix() ;

		// middle
		glPushMatrix() ;
		glTranslated( 0.0 , 0.0 , 1.0 ) ;
		glScaled( 1.5 , 1.5 , 1.0 ) ;
		this->cylinder->draw() ;
		glPopMatrix() ;

		// top
		glPushMatrix() ;
		glTranslated( 0.0 , 0.0 , 2.0 ) ;
		this->cylinder->draw() ;
		glPopMatrix() ;

		//glPopName() ;
		glPopMatrix() ;
	}
} ;


#endif