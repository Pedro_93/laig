#ifndef GAME_H
#define GAME_H

#include "Scene.h"
#include "Tabuleiro.h"
#include "BoardPieces.h"
#include "BoardSpot.h"
#include "Animation.h"
#include "GameCam.h"

class Game: public Scene {
private:
	CGFlight* light0;
	CGFlight* light1;

	BoardPiece * p1;
	BoardPiece * p2;
	BoardPiece * p3;

	Animation * anime;

	BoardSpot * bs ;
	BoardBorder * bb ;
	const int dimension ;

	float BOARD_AMBIENT[4] ;
	float BOARD_SPECULAR[4];
	float BOARD_DIFFUSE[4];

	UnitCube * tag ;
	CGFappearance * player1_tag ;
	CGFappearance * player2_tag ;
	CGFappearance * newHs_tag ;
	CGFappearance * charApp ;
	CGFappearance * enter_tag ;
	CGFappearance * replay ;
	CGFappearance * newGame;

	Tabuleiro * tab;
	bool SelectedFirstCoor,pieceSelected;
	int selectedXi,selectedXf,selectedYi,selectedYf,bonusX,bonusY;
	bool GameEnded,camRotation;

	int bonusInUse,movieIndex ;

	camera* gameCamera;

	void drawWithRoundedCorners();
	void drawWithRectCorners();
	void drawGoals();
	void drawTable(int n, int b);
	void drawBody();
	void drawGoalBorder(int rounded);
	void drawBoardBorders(int rounded);
	void drawPlayerTag();
	void drawPieces();
	void drawTime();
	void drawGoalPiece(int n);
	void drawTextBox();
	void drawBackButton() ;
	void drawMovie();
	void startServer();
	void closeServer();
	void setAppearance( int App ) ;
public:
	Game(int n , string spot1 , string spot2 , string border1 , string border2);
	~Game();
	string getName(){return "Game";}
	void display();
	void update(unsigned long t);
	int pickedValue(int Value,int parentCalled=1);
	void keyPressed(char key);
	void init();
	void leave();
	void movePiece(int Xi, int Yi,int Xf, int Yf);
	void AnimatedMove(Move* m);
	void startMovieAnimation();
	bool nextMovieMove();
	void changeView();
	void IAMove();
} ;

#endif