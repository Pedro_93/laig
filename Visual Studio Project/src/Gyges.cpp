#include "Gyges.h"
using namespace std;
long camera::LastNumber =2 ;
Gyges::Gyges(){
	states[MENU] = new Menu();
	states[PLAY] = new Game(6,"../data/solid/white.jpg","../data/solid/red.jpg","../data/solid/red.jpg", "../data/solid/brown.jpg");
	states[CONFIG] =new ConfMenu();//new ConfigWindow();
	states[HIGH] = new HSMenu();//new HighScore();
	ActiveState=MENU;
	currentScene=states[ActiveState];
};

Gyges::~Gyges(void){delete(currentScene);};

void Gyges::changeState(int valor)
{
	if (currentScene)
	{
		currentScene->leave();
	}
	ActiveState=valor;
	currentScene = states[ActiveState];
	currentScene->init();
}

void Gyges::init(){
	// Enables lighting computations
	glEnable(GL_LIGHTING);

	// Sets up some lighting parameters
	glLightModelf(GL_LIGHT_MODEL_TWO_SIDE, GL_FALSE);
	glLightModelfv(GL_LIGHT_MODEL_AMBIENT, CGFlight::background_ambient);  // Define ambient light
	glNormal3f(0,0,1);
	setUpdatePeriod(30);
	currentScene->init();
}

void Gyges::update(unsigned long millis)
{
	currentScene->update(millis);
}

void Gyges::display(){

	// ---- BEGIN Background, camera and axis setup

	// Clear image and depth buffer everytime we update the scene
	glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

	// Initialize Model-View matrix as identity (no transformation
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	// Apply transformations corresponding to the camera position relative to the origin

	currentScene->display();
	glutSwapBuffers();
}

void Gyges::pickedValue(int valor)
{
	int ret=currentScene->pickedValue(valor);
	if(ret!=ActiveState && ret!=4)
	{
		changeState(valor);
	}
	if(ret==4)
	{
		cout << "THANK YOU FOR PLAYING!\n";
		exit(0);
	}
}

void Gyges::keyPressed(char key)
{
	currentScene->keyPressed(key);
}